"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MainServicesService = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("../../environments/environment");
var RecordRTC = require("recordrtc");
var MainServicesService = /** @class */ (function () {
    function MainServicesService(http, adminGard, schoolG, hresurces, sVisionG, teacherG, router, 
    //private socket: Socket
    socket) {
        this.http = http;
        this.adminGard = adminGard;
        this.schoolG = schoolG;
        this.hresurces = hresurces;
        this.sVisionG = sVisionG;
        this.teacherG = teacherG;
        this.router = router;
        this.socket = socket;
        this.roonAndName = {};
        this.roomData = {};
        this.peers = {};
        this.enableRecordings = false;
        this.sCamera = [];
        this.url = environment_1.environment.wsUrl;
        this.checkStatus();
    }
    MainServicesService.prototype.ngOnInit = function () {
        this.download();
    };
    // check status
    MainServicesService.prototype.checkStatus = function () {
        var _this = this;
        this.socket.on("connect", function () {
            console.log("Conectado al servidor");
            _this.socketStatus = true;
        });
        this.socket.on("disconnect", function () {
            console.log("Desconectado del servidor");
            _this.socketStatus = false;
        });
    };
    MainServicesService.prototype.chatConnection = function (user, messages, data) {
        if (messages === void 0) { messages = []; }
        if (user !== undefined) {
            this.socket.emit("enterToRoom", user, function (resp) {
                console.log('enterToRoom ==>', resp);
                return data.emit(resp);
            });
            this.socket.on("privateMessage", function (message) {
                console.log("private Message:", message);
            });
        }
        // Escuchar información del servidor
        this.socket.on("createMessage", function (message) {
            messages.push(message);
        });
        //when someone left the Room
        this.socket.on("peopleList", function (people) {
            console.log('peopleList ==>', people);
            return data.emit(people);
        });
    };
    //login controller
    MainServicesService.prototype.postCanvas = function () {
        try {
            return this.http.post(this.url + "/2fa", {
                headers: { "Content-Type": "application/x-www-form-urlencoded" }
            });
        }
        catch (err) {
            console.error(err);
        }
    };
    MainServicesService.prototype.postLogin = function (email, password, sms, userId) {
        try {
            return this.http.post(this.url + "/login", {
                email: email,
                password: password,
                sms: sms,
                userId: userId,
                headers: { "Content-Type": "application/json" }
            });
        }
        catch (err) {
            console.error(err);
        }
    };
    MainServicesService.prototype.getTokenRoleRoom = function (TaRR) {
        console.log("TaRR", TaRR);
        this.roomData = TaRR;
        this.oks = TaRR["ok"];
        switch (TaRR["role"]) {
            case "ADMIN_ROLE":
                this.adminGard.Auth(this.oks);
                return this.router.navigateByUrl("/admin");
                break;
            case "HHRR_ROLE":
                this.hresurces.Auth(this.oks);
                return this.router.navigateByUrl("/hhrr");
                break;
            case "SVISION_ROLE":
                this.sVisionG.Auth(this.oks);
                return this.router.navigateByUrl("/svision");
                break;
            case "TEACHER_ROLE":
                this.teacherG.Auth(this.oks);
                this.getNameAndRoom(TaRR["name"], TaRR["room"], TaRR['id']);
                return this.router.navigateByUrl("/teachers");
                break;
            case "USER_ROLE":
                this.schoolG.Auth(this.oks);
                this.getNameAndRoom(TaRR["name"], TaRR["room"], TaRR['id']);
                return this.router.navigateByUrl("/school");
                break;
            default:
                return this.router.navigateByUrl("/home");
        }
    };
    MainServicesService.prototype.getNameAndRoom = function (name, room, id) {
        this.roonAndName = { name: name, room: room, id: id };
    };
    MainServicesService.prototype.getTaRR = function () {
        return this.roomData;
    };
    MainServicesService.prototype.download = function () {
        try {
            return this.http.get(this.url + "/download", {
                headers: { "Content-Type": "application/json" }
            });
        }
        catch (err) {
            console.error(err);
        }
    };
    // teacher toolbar controller
    MainServicesService.prototype.Camera = function (isCamera) {
        console.log(isCamera);
        this.socket.emit("isCamera", isCamera);
    };
    MainServicesService.prototype.Blackboard = function (isBlackboard) {
        this.socket.emit('isBlackboard', isBlackboard);
    };
    MainServicesService.prototype.Chat = function (isChat) {
        this.socket.emit('isChat', isChat);
    };
    MainServicesService.prototype.bBonTitle = function (title) {
        this.socket.emit("bBTitle", title);
    };
    // webRtc
    MainServicesService.prototype.handleEvent = function (connection, broadcastId) {
        var videoPreview = document.getElementById("video-preview");
        this.getSocket(broadcastId, connection);
        this.onLeaveIt(connection, videoPreview);
        connection.onstreamended = function () { };
        connection.onstream = function (event) {
            console.log(event);
            if (connection.isInitiator && event.type !== "local") {
                return;
            }
            connection.isUpperUserLeft = false;
            videoPreview.srcObject = event.stream;
            videoPreview.play();
            videoPreview["userid"] = event.userid;
            if (event.type === "local") {
                videoPreview.muted = true;
            }
            if (connection.isInitiator === false && event.type === "remote") {
                // he is merely relaying the media
                console.log(event);
                connection.dontCaptureUserMedia = true;
                connection.attachStreams = [event.stream];
                connection.sdpConstraints.mandatory = {
                    OfferToReceiveAudio: false,
                    OfferToReceiveVideo: false
                };
                connection.getSocket(function (socket) {
                    socket.emit("can-relay-broadcast");
                    console.log(socket);
                    if (connection.DetectRTC.browser.name === "Chrome") {
                        connection.getAllParticipants().forEach(function (p) {
                            console.log(p);
                            if (p + "" != event.userid + "") {
                                var peer_1 = connection.peers[p].peer;
                                peer_1.getLocalStreams().forEach(function (localStream) {
                                    console.log(localStream);
                                    peer_1.removeStream(localStream);
                                });
                                event.stream.getTracks().forEach(function (track) {
                                    peer_1.addTrack(track, event.stream);
                                });
                                connection.dontAttachStream = true;
                                connection.renegotiate(p);
                                connection.dontAttachStream = false;
                            }
                        });
                    }
                    console.log(connection);
                    if (connection.DetectRTC.browser.name === "Firefox") {
                        connection.getAllParticipants().forEach(function (p) {
                            if (p + "" != event.userid + "") {
                                connection.replaceTrack(event.stream, p);
                            }
                        });
                    }
                    if (connection.DetectRTC.browser.name === "Chrome") {
                        this.repeatedlyRecordStream(event.stream);
                    }
                });
            }
            // to keep room-id in cache
            localStorage.setItem(connection.socketMessageEvent, connection.sessionid);
        };
        // this.onLeaveIt(connection, videoPreview);
    };
    MainServicesService.prototype.repeatedlyRecordStream = function (stream, connection) {
        var allRecordedBlobs = [];
        console.log(stream, connection);
        if (!this.enableRecordings) {
            return;
        }
        connection.currentRecorder = RecordRTC(stream, {
            type: "video"
        });
        connection.currentRecorder.startRecording();
        setTimeout(function () {
            if (connection.isUpperUserLeft || !connection.currentRecorder) {
                return;
            }
            connection.currentRecorder.stopRecording(function () {
                allRecordedBlobs.push(connection.currentRecorder.getBlob());
                if (connection.isUpperUserLeft) {
                    return;
                }
                connection.currentRecorder = null;
                this.repeatedlyRecordStream(stream);
            });
        }, 30 * 1500); // 30-seconds
    };
    MainServicesService.prototype.onLeaveIt = function (connection, videoPreview) {
        console.log(connection, videoPreview);
        connection.onleave = function (event) {
            if (event.userid !== videoPreview.userid)
                return;
            connection.getSocket(function (socket) {
                socket.emit("can-not-relay-broadcast");
                connection.isUpperUserLeft = true;
                if (this.allRecordedBlobs.length) {
                    // playing lats recorded blob
                    var lastBlob = this.allRecordedBlobs[this.allRecordedBlobs.length - 1];
                    videoPreview.src = URL.createObjectURL(lastBlob);
                    videoPreview.play();
                    this.allRecordedBlobs = [];
                }
                else if (connection.currentRecorder) {
                    var recorder_1 = connection.currentRecorder;
                    connection.currentRecorder = null;
                    recorder_1.stopRecording(function () {
                        if (!connection.isUpperUserLeft)
                            return;
                        videoPreview.src = URL.createObjectURL(recorder_1.getBlob());
                        videoPreview.play();
                    });
                }
                if (connection.currentRecorder) {
                    connection.currentRecorder.stopRecording();
                    connection.currentRecorder = null;
                }
            });
        };
    };
    MainServicesService.prototype.getSocket = function (broadcastId, connection) {
        connection.extra.broadcastId = broadcastId;
        connection.session = {
            audio: true,
            video: true,
            oneway: true
        };
        connection.getSocket(function (socket) {
            socket.emit("check-broadcast-presence", broadcastId, function (isBroadcastExists) {
                if (!isBroadcastExists) {
                    // the first person (i.e. real-broadcaster) MUST set his user-id
                    connection.userid = broadcastId;
                }
                console.log("check-broadcast-presence", broadcastId, isBroadcastExists);
                socket.emit("join-broadcast", {
                    broadcastId: broadcastId,
                    userid: connection.userid,
                    typeOfStreams: connection.session
                });
            });
        });
    };
    MainServicesService.prototype.logOut = function () {
        this.oks;
        this.roonAndName = {};
        this.adminGard.Auth(false);
        this.hresurces.Auth(false);
        this.sVisionG.Auth(false);
        this.teacherG.Auth(false);
        this.schoolG.Auth(false);
    };
    MainServicesService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], MainServicesService);
    return MainServicesService;
}());
exports.MainServicesService = MainServicesService;
