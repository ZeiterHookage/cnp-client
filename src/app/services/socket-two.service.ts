
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketTwoService  extends Socket{

  constructor() {
    super({ url: environment.wsUrl, options: { origin: '*', transport : ['websocket'] } });
   }
}
