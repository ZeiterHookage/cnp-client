
import {  EventEmitter, Injectable, OnInit } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import * as RecordRTC from "recordrtc";
//import * as io from "socket.io-client";
import {
  AdminGuard,
  RegisterGuard,
  SchoolGuard,
  HumanResorcesGuard,
  SupervisionGuard,
  TeachersGuard,
} from "./Main.guard";
import { Router, ActivatedRoute } from "@angular/router";
import Peer from "peerjs";
import RTCMulticonnection from "rtcmulticonnection"
import { Socket } from "ngx-socket-io";
@Injectable({
  providedIn: "root",
})
export class MainServicesService implements OnInit {
  socketStatus;

  private _isCamera: boolean;
  oks: boolean;
  roonAndName: any = {};
  roomData: any = {};
  myVideo: any;
  peers = {};
  myPeer: Peer;
  uuid: any;
  enableRecordings = false;
  connection: any;
  sCamera: any[] = [];
  subscription: any;
  url = environment.wsUrl;
  events: any;

  private _myEvent: any;

  private _events: any;
  constructor(
    private http: HttpClient,
    private adminGard: AdminGuard,
    private schoolG: SchoolGuard,
    private hresurces: HumanResorcesGuard,
    private sVisionG: SupervisionGuard,
    private teacherG: TeachersGuard,
    private router: Router,
    //private socket: Socket
    private socket: Socket
  ) {
    this.checkStatus();
  }

  ngOnInit(){
  this.download();
  }



  // check status
  checkStatus() {
    this.socket.on("connect", () => {
      console.log("Conectado al servidor");
      this.socketStatus = true;
    });
    this.socket.on("disconnect", () => {
      console.log("Desconectado del servidor");
      this.socketStatus = false;
    });
  }

  chatConnection(user: any,messages:any=[], data: EventEmitter<any>) {

    if (user !== undefined) {
      this.socket.emit("enterToRoom", user, (resp) => {
          console.log('enterToRoom ==>',resp);
        return data.emit(resp);
      });


      this.socket.on("privateMessage", (message: any) => {
        console.log("private Message:", message);
      });
    }
    // Escuchar información del servidor
    this.socket.on("createMessage", (message) => {

      messages.push(message);
    });


    //when someone left the Room
    this.socket.on("peopleList", (people) => {
      console.log('peopleList ==>',people);
      return data.emit(people);
    });

  }
  //login controller
  postCanvas() {
    try {
      return this.http.post<any>(`${this.url}/2fa`, {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
      });
    } catch (err) {
      console.error(err);
    }
  }
postLogin(email: string, password: string,sms:any,userId:any) {
    try {
      return this.http.post<any>(`${this.url}/login`, {
        email,
        password,
        sms,
        userId,

        headers: { "Content-Type": "application/json" },
      });
    } catch (err) {
      console.error(err);
    }
  }




  getTokenRoleRoom(TaRR: object) {
    console.log("TaRR", TaRR);
    this.roomData = TaRR;
    this.oks = TaRR["ok"];
    switch (TaRR["role"]) {
      case "ADMIN_ROLE":
        this.adminGard.Auth(this.oks);
        return this.router.navigateByUrl("/admin");
        break;

      case "HHRR_ROLE":
        this.hresurces.Auth(this.oks);
        return this.router.navigateByUrl("/hhrr");
        break;

      case "SVISION_ROLE":
        this.sVisionG.Auth(this.oks);
        return this.router.navigateByUrl("/svision");
        break;

      case "TEACHER_ROLE":
        this.teacherG.Auth(this.oks);

        this.getNameAndRoom(TaRR["name"], TaRR["room"],TaRR['id']);

        return this.router.navigateByUrl("/teachers");
        break;

      case "USER_ROLE":
        this.schoolG.Auth(this.oks);
        this.getNameAndRoom(TaRR["name"], TaRR["room"],TaRR['id']);
        return this.router.navigateByUrl("/school");
        break;

      default:
        return this.router.navigateByUrl("/home");
    }
  }
  getNameAndRoom(name: string, room: string,id:any) {
    this.roonAndName = { name, room, id };
  }
  getTaRR() {
    return this.roomData;
  }

  download() {
    try {
      return this.http.get<any>(`${this.url}/download`, {
        headers: { "Content-Type": "application/json" },
      });
    } catch (err) {
      console.error(err);
    }
  }
  // teacher toolbar controller
  Camera(isCamera: any) {
    console.log(isCamera)
    this.socket.emit("isCamera", isCamera);
  }
  Blackboard(isBlackboard: any) {
    this.socket.emit('isBlackboard', isBlackboard);
  }

  Chat(isChat: any) {
    this.socket.emit('isChat', isChat);
  }
  bBonTitle(title: string) {
    this.socket.emit("bBTitle", title);
  }







  // webRtc
  handleEvent(connection: RTCMulticonnection, broadcastId: any) {
    const videoPreview = document.getElementById(
      "video-preview"
    ) as HTMLMediaElement;

    this.getSocket(broadcastId, connection);
    this.onLeaveIt(connection, videoPreview);

    connection.onstreamended = function () {};

    connection.onstream = function (event) {
      console.log(event);
      if (connection.isInitiator && event.type !== "local") {
        return;
      }

      connection.isUpperUserLeft = false;
      videoPreview.srcObject = event.stream;
      videoPreview.play();

      videoPreview["userid"] = event.userid;

      if (event.type === "local") {
        videoPreview.muted = true;
      }

      if (connection.isInitiator === false && event.type === "remote") {
        // he is merely relaying the media
        console.log(event);

        connection.dontCaptureUserMedia = true;
        connection.attachStreams = [event.stream];
        connection.sdpConstraints.mandatory = {
          OfferToReceiveAudio: false,
          OfferToReceiveVideo: false,
        };

        connection.getSocket(function (socket) {
          socket.emit("can-relay-broadcast");
          console.log(socket);
          if (connection.DetectRTC.browser.name === "Chrome") {
            connection.getAllParticipants().forEach(function (p) {
              console.log(p);
              if (p + "" != event.userid + "") {
                let peer = connection.peers[p].peer;
                peer.getLocalStreams().forEach(function (localStream) {
                  console.log(localStream);
                  peer.removeStream(localStream);
                });
                event.stream.getTracks().forEach(function (track) {
                  peer.addTrack(track, event.stream);
                });
                connection.dontAttachStream = true;
                connection.renegotiate(p);
                connection.dontAttachStream = false;
              }
            });
          }
          console.log(connection);
          if (connection.DetectRTC.browser.name === "Firefox") {
            connection.getAllParticipants().forEach(function (p) {
              if (p + "" != event.userid + "") {
                connection.replaceTrack(event.stream, p);
              }
            });
          }

          if (connection.DetectRTC.browser.name === "Chrome") {
            this.repeatedlyRecordStream(event.stream);
          }
        });
      }

      // to keep room-id in cache
      localStorage.setItem(connection.socketMessageEvent, connection.sessionid);
    };

    // this.onLeaveIt(connection, videoPreview);
  }

  repeatedlyRecordStream(stream: any, connection: any) {
    const allRecordedBlobs = [];
    console.log(stream, connection);
    if (!this.enableRecordings) {
      return;
    }

    connection.currentRecorder = RecordRTC(stream, {
      type: "video",
    });

    connection.currentRecorder.startRecording();

    setTimeout(function () {
      if (connection.isUpperUserLeft || !connection.currentRecorder) {
        return;
      }

      connection.currentRecorder.stopRecording(function () {
        allRecordedBlobs.push(connection.currentRecorder.getBlob());

        if (connection.isUpperUserLeft) {
          return;
        }

        connection.currentRecorder = null;
        this.repeatedlyRecordStream(stream);
      });
    }, 30 * 1500); // 30-seconds
  }

  onLeaveIt(connection: RTCMulticonnection, videoPreview: any) {
    console.log(connection, videoPreview);
    connection.onleave = function (event) {
      if (event.userid !== videoPreview.userid) return;

      connection.getSocket(function (socket) {
        socket.emit("can-not-relay-broadcast");

        connection.isUpperUserLeft = true;

        if (this.allRecordedBlobs.length) {
          // playing lats recorded blob
          let lastBlob = this.allRecordedBlobs[
            this.allRecordedBlobs.length - 1
          ];
          videoPreview.src = URL.createObjectURL(lastBlob);
          videoPreview.play();
          this.allRecordedBlobs = [];
        } else if (connection.currentRecorder) {
          let recorder = connection.currentRecorder;
          connection.currentRecorder = null;
          recorder.stopRecording(function () {
            if (!connection.isUpperUserLeft) return;

            videoPreview.src = URL.createObjectURL(recorder.getBlob());
            videoPreview.play();
          });
        }

        if (connection.currentRecorder) {
          connection.currentRecorder.stopRecording();
          connection.currentRecorder = null;
        }
      });
    };
  }

  getSocket(broadcastId: any, connection: RTCMulticonnection) {
    connection.extra.broadcastId = broadcastId;

    connection.session = {
      audio: true,
      video: true,
      oneway: true,
    };

    connection.getSocket(function (socket) {
      socket.emit(
        "check-broadcast-presence",
        broadcastId,
        (isBroadcastExists) => {
          if (!isBroadcastExists) {
            // the first person (i.e. real-broadcaster) MUST set his user-id
            connection.userid = broadcastId;
          }

          console.log(
            "check-broadcast-presence",
            broadcastId,
            isBroadcastExists
          );

          socket.emit("join-broadcast", {
            broadcastId,
            userid: connection.userid,
            typeOfStreams: connection.session,
          });
        }
      );
    });
  }

  logOut() {
    this.oks;
    this.roonAndName = {};
    this.adminGard.Auth(false);
    this.hresurces.Auth(false);
    this.sVisionG.Auth(false);
    this.teacherG.Auth(false);
    this.schoolG.Auth(false);
  }
}
