import { EventEmitter, Input, Output,OnInit,Component } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import {fabric} from 'fabric';
@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class ColorPickerComponent implements OnInit {
  @Input() heading: string;
  @Input() color: string;
  @Output() eventColor = new EventEmitter();
  @Input()Pcanvas:any;
  @Input()Pctx:any;
  public show = false;
  public selectedColor = "black";
  activeObject: fabric.Object;
  public defaultColors: string[] = [
    '#ffffff',
    '#000105',
    '#3e6158',
    '#3f7a89',
    '#96c582',
    '#b7d5c4',
    '#bcd6e7',
    '#7c90c1',
    '#9d8594',
    '#dad0d8',
    '#4b4fce',
    '#4e0a77',
    '#a367b5',
    '#ee3e6d',
    '#d63d62',
    '#c6a670',
    '#f46600',
    '#cf0500',
    '#efabbd',
    '#8e0622',
    '#f0b89a',
    '#f0ca68',
    '#62382f',
    '#c97545',
    '#c1800b',
    'black',
    'red',
    'yellow',
    'blue',
    'green',
    'white',
    'brown',
    'orange',
    'pulple',
    '#006400'



  ];
  constructor(private socket: Socket) { }

  ngOnInit(): void {

  }

  ChangeColor(colors) {

    this.color = colors;
    this.eventColor.emit(this.color);

    this.selectedColor = this.color;
    this.show = false;
  }
  public toggleColors() {
    this.show = !this.show;
  }

  changeColorOutside() {
    this.socket.on("lineColor", (colorLine) => {
       this.selectedColor = colorLine;
       new fabric.Color(this.selectedColor);

    });
  }

}
