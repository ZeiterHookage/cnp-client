import { MainServicesService } from "src/app/services/main-services.service";
import { environment } from "src/environments/environment";
import {
  AfterContentChecked,
  Component,
  OnInit,
  AfterViewChecked,
  Directive,
  HostListener,
  ElementRef,
  Input,
} from "@angular/core";
import { Subject, Subscription } from "rxjs";
import { SocketTwoService } from "src/app/services/socket-two.service";
import { Socket } from "ngx-socket-io";
import {fabric} from 'fabric';
@Component({
  selector: "app-blackboard",
  templateUrl: "./blackboard.component.html",
  styleUrls: ["./blackboard.component.css"],
})
export class BlackboardComponent implements OnInit {
  @Input() isTeacherClassBlackB: any;
  @Input() title: any;
  //socket: any;
  url: any;
  canvas: any;
  ctx: any;
  pimg: any;
  pattern: any;
  imgInput: any;
  // tslint:disable-next-line:variable-name
  curr_color: any;
  dctrl: any = {};
  //message:any;
  // toolbar
  public toolbarHidden = false;
  public confirmDeleteBoxVisible = false;
  //public pencilIcon = ['point-sm'];
  public colorPaletteBoxVisible = false;
  public selectedColor = "black";
  isErase = false;
  data = new Subject<any>();
  myObservableArray = this.data.asObservable();
  subscription: Subscription = new Subscription();
  myImage: any;
  image: any;
  //
  constructor(
    private mainService: MainServicesService,
    private socket: Socket
  ) {
    /* this.url = environment.wsUrl;
    this.socket = io(this.url);*/
  }

  ngOnInit() {
    this.pimg = new Image();
    this.pimg.onload = this.init();
    this.pimg.src = "/src/assets/img/tiza.png";
    this.onFMousedown();
    this.onFMouseMove();
    this.onFMouseUp();
    this.changeColorOutside();
    this.Erasing();
  }

  init() {
    Object.defineProperties(this.dctrl, {
      drawing: {
        enumerable: true,
        configurable: true,
        writable: true,
        value: false,
      },
      prevx: {
        enumerable: true,
        configurable: true,
        writable: true,
        value: 0,
      },
      prevy: {
        enumerable: true,
        configurable: true,
        writable: true,
        value: 0,
      },
    });
    this.canvas = document.getElementById("blackboards");
    //this.canvas= new fabric.Canvas('blackboards', );

    // this.imgInput = document.getElementById("imageInput");
    if (!this.canvas) {
      alert('Error! The canvas element was not found!');
      return;
      }

    this.canvas.style.cursor = "default";

    this.ctx = this.canvas.getContext("2d");
    if (!this.ctx) {
      alert('Error! No canvas.getContext!');
      return;
      }
    this.ctx.beginPath();
    this.withAndHeight(this.canvas);

    this.ctx.lineWidth = 1;
    this.ctx.lineCap = "round";
    this.pattern = this.ctx.createPattern(this.pimg, "repeat");
    if (this.isTeacherClassBlackB === false) {
      this.curr_color = "#6FC";
      this.ctx.font = "bold 48px sans-serif";
      this.ctx.textAlign = "center";
    } else {
      this.curr_color = "#6FC";
      this.ctx.font = "bold 36px sans-serif";
      this.ctx.textAlign = "center";
    }
    this.ctx.globalCompositeOperation = "source-over";
    this.ctx.strokeStyle = this.curr_color;
    this.ctx.strokeText(this.title, this.canvas.width / 2, 40);
    this.ctx.globalCompositeOperation = "destination-over";
    this.ctx.strokeText(this.title, this.canvas.width / 2, 40);

  }

  ClearCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  EraseCanvas() {
    this.isErase = !this.isErase;
    console.log(this.isErase);
    if(this.isErase){
      this.ctx.globalCompositeOperation = 'destination-out';
      this.ctx.lineWidth = 10;
    }
    this.socket.emit("erase", this.isErase);
  }

  //events
  @HostListener("mousedown", ["$event"]) mouseDown(event: ElementRef) {
    if (event !== undefined) {
      let obj = {
        offsetX: event['offsetX'],
        offsetY: event['offsetY'],
        clientX: event['clientX'],
        clientY: event['clientY'],
        target: event['target'].getBoundingClientRect(),
        view: event['view'].location.pathname,
      };
      this.socket.emit("hotListenerMousedown", obj);
       this.fMousedown(obj);
    }


  }

  fMousedown(event: any) {
    this.dctrl["prevx"] = event["offsetX"];
    this.dctrl["prevy"] = event["offsetY"];
    let rect = event["target"];
    this.dctrl["drawing"] = true;
  }
  onFMousedown() {
    this.socket
      .fromEvent("hotListenerMousedown")
      .subscribe((event) => this.fMousedown(event));
  }

  @HostListener("mousemove", ["$event"]) mouseMove(event: ElementRef) {
    // falta definir limites.
    if (event !== undefined) {
      let obj = {
        offsetX: event.offsetX,
        offsetY: event.offsetY,
        clientX: event.clientX,
        clientY: event.clientY,
        target: event.target.getBoundingClientRect(),
        view: event.view.location.pathname,
      };
      this.socket.emit("hotListenerMousemove", obj);
    this.fMouseMove(obj);
    }

  }

  fMouseMove(event: any) {
    if (this.dctrl["drawing"]) {
      this.draw_line_ev(event);
      this.dctrl["prevx"] = event["offsetX"];
      this.dctrl["prevy"] = event["offsetY"];
    }
  }

  onFMouseMove() {
    this.socket.fromEvent("hotListenerMousemove").subscribe((event) => {
      this.fMouseMove(event);
    });
  }

  @HostListener("mouseout", ["$event"]) mouseOut(event: ElementRef) {
    this.dctrl["drawing"] = false;

    this.fMouseout();
  }

  fMouseout() {
    this.dctrl["drawing"] = false;
  }

  onFMouseout() {
    this.socket
      .fromEvent("hotListenerMouseout")
      .subscribe((event) => this.fMouseout(event));
  }

  @HostListener("mouseup", ["$event"]) mouseUp(event: ElementRef) {
    if (event !== undefined) {
      let obj = {
        offsetX: event.offsetX,
        offsetY: event.offsetY,
        clientX: event.clientX,
        clientY: event.clientY,
        target: event.target.getBoundingClientRect(),
        view: event.view.location.pathname,
      };
      this.socket.emit("hotListenerMouseup", obj);
    }
    this.fMouseUp(obj);
  }

  fMouseUp(event: any) {
    if (this.dctrl["drawing"]) {
      this.draw_line_ev(event);
      this.dctrl["prevx"] = 0;
      this.dctrl["prevy"] = 0;
      this.dctrl["drawing"] = false;
    }
  }
  onFMouseUp() {
    this.socket
      .fromEvent("hotListenerMouseup")
      .subscribe((event) => this.fMouseUp(event));
  }

  draw_line(x1, y1, x2, y2, ev) {

    this.ctx.beginPath();
const spandingX = 1.3;
      const spandingY = 1.056;
      let iX: number;
      let iY: number;
      let iX2: number;
      let iY2: number;
    if (ev["view"] === "/teachers" && this.isTeacherClassBlackB === false) {
        if(x1< this.canvas.width || y1 < this.canvas.height)
        {
          iX = x1 * spandingX;
        iY = y1 * spandingY;
        iX2 = x2 * spandingX;
        iY2 = y2 * spandingY
        this.ctx.moveTo(iX, iY);
       this.ctx.lineTo(iX2, iY2);
}else{
  this.dctrl['drawing']= false;
  this.dctrl["prevx"] = 0;
  this.dctrl["prevy"] = 0;
  this.ctx.moveTo(0, 0);
  this.ctx.lineTo(0, 0);
}
    } else if (ev["view"] === "/school" && this.isTeacherClassBlackB === true) {
      if(x1< this.canvas.width || y1 < this.canvas.height)
       { iX = x1 / spandingX;
        iY = y1 / spandingY;
        iX2 = x2 / spandingX;
        iY2 = y2 / spandingY;
        this.ctx.moveTo(iX, iY);
       this.ctx.lineTo(iX2, iY2);}
       else{
        this.dctrl["prevx"] = 0;
        this.dctrl["prevy"] = 0;
        this.dctrl['drawing']= false;
        this.ctx.moveTo(0, 0);
        this.ctx.lineTo(0, 0);
       }
    } else {
      this.ctx.moveTo(x1, y1);
      this.ctx.lineTo(x2, y2);
    }

   /* this.ctx.globalCompositeOperation = "source-over";
   /* this.ctx.strokeStyle = this.curr_color;
    this.ctx.stroke();
    *this.ctx.globalCompositeOperation = "destination-out";*/
    /*this.ctx.strokeStyle = this.pattern;*/
    this.ctx.stroke();
  }

  draw_line_ev(ev) {
    let rect = ev.target;
    let mousex = ev["clientX"] - rect["left"];
    let mousey = ev["clientY"] - rect["top"];
    this.draw_line(
      this.dctrl["prevx"],
      this.dctrl["prevy"],
      mousex,
      mousey,
      ev
    );
    this.dctrl["prevx"] = mousex;
    this.dctrl["prevy"] = mousey;
  }

  ShowPaletteColorBox(event) {
    this.colorPaletteBoxVisible = true;
    // show the box where the mouse is (add the height of the box to y coord)
  }

  // change line weight
  ChangeLineWidth() {
    this.ctx.lineWidth += 1;
    this.socket.emit("lineWidth", this.ctx.lineWidth);
  }
  ChangeLineMinus() {
    if (this.ctx.lineWidth > 1) {
      this.ctx.lineWidth -= 1;
      this.socket.emit("lineWidth", this.ctx.lineWidth);
    } else {
      return;
    }
  }

  ChangeColor(colors) {
    console.log(colors);
    this.ctx.strokeStyle = colors;
    this.selectedColor = colors;
    this.colorPaletteBoxVisible = false;
    this.socket.emit("lineColor", colors);
  }

  changeColorOutside() {
    this.socket.on("lineColor", (colorLine) => {
      this.ctx.strokeStyle = colorLine;
      this.selectedColor = colorLine;
    });
  }
  changeLineOutside() {
    this.socket.on(
      "lineWidth",
      (lineWidth) => (this.ctx.lineWidth = lineWidth)
    );
  }

  Erasing() {
    this.socket.on("erase", (eraseObj) => {
      console.log(eraseObj);

      this.isErase = eraseObj;
      this.ctx.globalCompositeOperation = 'destination-out';
      this.ctx.lineWidth = 10;
    });
  }

  loadingImg(canvas: any, ctx: any) {
    this.socket.on("Image", (Image) => {
     this.image = new Blob([new Uint8Array(Image).buffer]);
      console.log(this.image);
      // this.fLoadingIMg(image, canvas, ctx);
    });
  }
  fLoadingIMg(imageFile: any, canvas: any, ctx: any) {
    const reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onloadend = (e) => {
      this.myImage = new Image(); // Creates image object
      this.myImage.src = e.target.result;
      // Assigns converted image to image object

      console.log("myImage", this.myImage);
      this.myImage.onload = (ev) => {
        console.log(ev);
        console.log(canvas);
        // Creates a contect object
        canvas["width"] = ev.path[0]["width"]; // Assigns image's width to canvas
        canvas["height"] = ev.path[0]["height"];

        // Assigns image's height to canvas
        this.withAndHeight(this.canvas);
        ctx.drawImage(this.myImage, 10, 10, 150, 150);

        this.init();
        // Draws the image on canvas
        //let imgData = this.canvas.toDataURL("image/jpeg"0); // Assigns image base64 string in jpeg format to a variable
      };
    };
  }
  withAndHeight(canvas: any) {
    if (this.isTeacherClassBlackB === false) {
      canvas.style.width = "1170px";
      canvas.style.height = "530px";
      canvas.width = "1170";
      canvas.height = "530";
    } else {
      canvas.style.width = "900px";
      canvas.style.height = "500px";
      canvas.width = "900";
      canvas.height = "500";
    }
    return canvas;
  }
}
