"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BlackboardComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var fabric_1 = require("fabric");
var fabric_history_1 = require("fabric-history");
// tslint:disable-next-line:no-namespace
var BlackboardComponent = /** @class */ (function () {
    function BlackboardComponent(socket, mainService) {
        this.socket = socket;
        this.mainService = mainService;
        this.mySubject = new rxjs_1.BehaviorSubject(false);
        this.drawingSubject = this.mySubject.asObservable();
        this.lastPosX = null;
        this.lastPosY = null;
    }
    BlackboardComponent.prototype.ngOnInit = function () {
        this.canvas = new fabric_1.fabric.Canvas("blackboards", {
            selection: true,
            renderOnAddRemove: true,
            includeDefaultValues: false
        });
        this.ctx = this.canvas.getContext("2d");
        this.canvas.selectionFullyContained = false;
        this.canvas.preserveObjectStacking = true;
        this.canvas.stopContextMenu = true;
        this.canvas.set({ history: fabric_history_1.history });
        this.withAndHeight(this.canvas);
        this.asignatureTitle();
        this.fEvents();
        this.renderBackGround();
    };
    BlackboardComponent.prototype.ngAfterViewInit = function () { };
    BlackboardComponent.prototype.readyCanvas = function (event) {
        this.canvas.isDrawingMode = event;
    };
    BlackboardComponent.prototype.asignatureTitle = function () {
        var title = new fabric_1.fabric.IText(this.title, {
            selectable: false,
            excludeFromExport: false,
            absolutePositioned: true
        });
        this.canvas.add(title).centerObjectH(title).set({ selectable: false });
    };
    BlackboardComponent.prototype.ClearCanvas = function () {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };
    BlackboardComponent.prototype.fEvents = function () {
        var _this = this;
        this.canvas.on('path:created', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('selection:updated', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('mouse:down', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('mouse:up', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('editing:entered', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('editing:exited', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('selection:changed', function () {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('object:rotating', function (e) {
            var room = _this.mainService.getTaRR();
            if (e['target']['__proto__']['type'] === 'image') {
                _this.canvas.off('object:rotating');
                _this.canvas.on('object:rotated', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
            else {
                _this.canvas.on('object:rotating', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
        });
        this.canvas.on('object:scaling', function (e) {
            var room = _this.mainService.getTaRR();
            if (e['target']['__proto__']['type'] === 'image') {
                _this.canvas.off('object:scaling');
                _this.canvas.on('object:scaled', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
            else {
                _this.canvas.on('object:scaling', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
        });
        this.canvas.on('object:moving', function (e) {
            var room = _this.mainService.getTaRR();
            if (e['target']['__proto__']['type'] === 'image') {
                _this.canvas.off('object:moving');
                _this.canvas.on('object:moved', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
            else {
                _this.canvas.on('object:moving', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
        });
        this.canvas.on('object:skewing', function (e) {
            var room = _this.mainService.getTaRR();
            if (e['target']['__proto__']['type'] === 'image') {
                _this.canvas.off('object:skewing');
                _this.canvas.on('object:skewed', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
            else {
                _this.canvas.on('object:skewing', function () {
                    _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
                });
            }
        });
        this.canvas.on('object:added', function (options) {
            options.target.set('added', true);
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        this.canvas.on('object:removed', function (options) {
            var room = _this.mainService.getTaRR();
            _this.socket.emit('refresh', { canvas: _this.stringifyCanvas(), room: room.room });
        });
        /*this.canvas.on('event:added',  (options)=> {
          const room = this.mainService.getTaRR();
         this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
       });
    
       this.canvas.on('event:selected',  (options)=> {
        const room = this.mainService.getTaRR();
       this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
     });
    
     this.canvas.on('event:deselected',  (options)=> {
      const room = this.mainService.getTaRR();
     this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
    });
    
    this.canvas.on('event:modified',  (options)=> {
      const room = this.mainService.getTaRR();
     this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
    });*/
        /* this.canvas.on('object:modified',  ()=> {
      
      this.socket.emit('refresh', this.emitting())
            let obj = e.target;
      
                      if (obj.getLeft() < 0
                          || obj.getTop() < 0
                          || obj.getLeft() + obj.getWidth() > this.canvas.getWidth()
                          || obj.getTop() + obj.getHeight() > this.canvas.getHeight()) {
                          if (obj.getAngle() != obj.originalState.angle) {
                              obj.setAngle(obj.originalState.angle);
                          }
                          else {
                              obj.setTop(obj.originalState.top);
                              obj.setLeft(obj.originalState.left);
                              obj.setScaleX(obj.originalState.scaleX);
                              obj.setScaleY(obj.originalState.scaleY);
                          }
                          obj.setCoords();
                      }else{
                        return
                      }
      
      
          });*/
    };
    BlackboardComponent.prototype.withAndHeight = function (canvas) {
        /*fabric.Image.fromURL("../../../assets/img/boardFront.png", function (img) {
          img.set({
            scaleX: canvas.width / img.width,
            scaleY: canvas.height / img.height,
          });
    
          canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
            excludeFromExport: true,
          });
        });*/
        canvas.setDimensions({
            width: 1170,
            height: 530,
            left: 17
        }, { backstoreOnly: false, cssOnly: false });
        /*if (this.isTeacherClassBlackB === false) {
          fabric.Image.fromURL(
            "../../../assets/img/boardFront.png",
            function (img) {
              img.set({
                scaleX: canvas.width / img.width,
                scaleY: canvas.height / img.height,
              });
    
              canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            }
          );
          canvas.setDimensions(
            {
              width: 1170,
              height: 530,
              left: 17,
            },
            { backstoreOnly: false, cssOnly: false }
          );
        } else {
          fabric.Image.fromURL(
            "../../../assets/img/boardFront.png",
            function (img) {
              img.set({
                scaleX: canvas.width / img.width,
                scaleY: canvas.height / img.height,
              });
              canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            }
          );
          canvas.setDimensions(
            {
              width: 900,
              height: 500,
              left: 17,
            },
            { backstoreOnly: false, cssOnly: false }
          );
        }*/
    };
    BlackboardComponent.prototype.stringifyCanvas = function () {
        return this.canvas.toJSON([
            'selectable', 'added', 'lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingFlip', 'lockScalingFlip',
            'lockScalingY', 'lockSkewingX', 'lockSkewingY',
        ]);
    };
    BlackboardComponent.prototype.renderBackGround = function () {
        var _this = this;
        this.socket.on('refresh', function (data) {
            console.log('data', data);
            _this.renderCanvas(data);
        });
    };
    BlackboardComponent.prototype.renderCanvas = function (data) {
        var _this = this;
        this.roomData = this.mainService.getTaRR();
        if (data.room === this.roomData['room']) {
            data.canvas.objects.forEach(function (element) {
                if (element['added']) {
                    _this.canvas.off('object:added');
                    _this.canvas.loadFromJSON(data.canvas, _this.canvas.renderAll.bind(_this.canvas));
                }
                else {
                    _this.canvas.loadFromJSON(data.canvas, _this.canvas.renderAll.bind(_this.canvas));
                }
            });
        }
    };
    __decorate([
        core_1.Input()
    ], BlackboardComponent.prototype, "isTeacherClassBlackB");
    __decorate([
        core_1.Input()
    ], BlackboardComponent.prototype, "title");
    BlackboardComponent = __decorate([
        core_1.Component({
            selector: "app-blackboard",
            templateUrl: "./blackboard.component.html",
            styleUrls: ["./blackboard.component.css"]
        })
    ], BlackboardComponent);
    return BlackboardComponent;
}());
exports.BlackboardComponent = BlackboardComponent;
