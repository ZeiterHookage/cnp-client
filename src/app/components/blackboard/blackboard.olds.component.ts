import { element } from "protractor";
import { SocketTwoService } from "./../../services/socket-two.service";
import { MainServicesService } from "src/app/services/main-services.service";
import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  Input,
  AfterViewInit,
} from "@angular/core";
import { Socket } from "ngx-socket-io";
import { environment } from "../../../environments/environment";
import { fromEventPattern, Observable, Subject, Subscription } from "rxjs";
import { SocketOneService } from "src/app/services/socket-one.service";
import { map } from "rxjs/operators";
@Component({
  selector: "app-blackboard",
  templateUrl: "./blackboard.component.html",
  styleUrls: ["./blackboard.component.css"],
})
export class BlackboardComponent implements OnInit, AfterViewInit {
  //socket1:any;
  @Input() isTeacherClassBlackB: any;
  @Input() title: any;

  canvas: any;
  ctx: any;
  pimg: any;
  pattern: any;
  imgInput: any;
  // tslint:disable-next-line:variable-name
  curr_color: any;
  dctrl: any = {};
  //message:any;
  // toolbar
  public toolbarHidden = false;
  public confirmDeleteBoxVisible = false;
  //public pencilIcon = ['point-sm'];
  public colorPaletteBoxVisible = false;
  public selectedColor = "black";
  isErase = false;
  data = new Subject<any>();
  myObservableArray = this.data.asObservable();
  subscription: Subscription = new Subscription();
  @Input() hotListenerMousedown: any;
  @Input() hotListenerMousemove: any;
  @Input() hotListenerMouseout: any;
  @Input() hotListenerMouseup: any;
  myImage: any;
  //
  constructor(
    private mainService: MainServicesService /*private socket: Socket,*/,
    private socket: SocketTwoService
  ) {}

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.pimg = new Image();
    this.pimg.onload = this.init();
    this.pimg.src = "../../../assets/img/tiza.png";
    this.Mousedown();
    this.MouseMove();
    this.MouseOut();
    this.MouseUp();
    this.changeColorOutside();
    this.changeLineOutside();
    this.Erasing();
  }
  ngAfterViewInit() {}

  // tslint:disable-next-line:typedef
  init() {
    Object.defineProperties(this.dctrl, {
      drawing: {
        enumerable: true,
        configurable: true,
        writable: true,
        value: false,
      },
      prevx: {
        enumerable: true,
        configurable: true,
        writable: true,
        value: 0,
      },
      prevy: {
        enumerable: true,
        configurable: true,
        writable: true,
        value: 0,
      },
    });
    this.canvas = document.getElementById("blackboards");
    this.imgInput = document.getElementById("imageInput");

    this.canvas.style.cursor = "default";

    this.ctx = this.canvas.getContext("2d");

    this.ctx.beginPath();
    this.withAndHeight(this.canvas);

    this.ctx.lineWidth = 1;
    this.ctx.lineCap = "round";
    this.pattern = this.ctx.createPattern(this.pimg, "repeat");
    this.curr_color = "#6FC";
    this.ctx.font = "bold 36px sans-serif";
    this.ctx.textAlign = "center";
    this.ctx.globalCompositeOperation = "source-over";
    this.ctx.strokeStyle = this.curr_color;
    this.ctx.strokeText(this.title, this.canvas.width / 2, 40);
    this.ctx.globalCompositeOperation = "destination-over";
    this.ctx.strokeText(this.title, this.canvas.width / 2, 40);
    /*this.loadImg(this.canvas, this.ctx);
    this.loadingImg(this.canvas, this.ctx);*/
  }

  draw_line(x1, y1, x2, y2, isDrawing: boolean) {
    this.ctx.beginPath();

    if (this.isErase === false) {
      this.ctx.globalCompositeOperation = "source-over";
      this.ctx.strokeStyle = this.selectedColor;
      this.ctx.strokeStyle = this.pattern;
      /*this.ctx.globalCompositeOperation = "destination-out";
       */
    } else {
      this.ctx.globalCompositeOperation = "destination-out";

      //
      this.ctx.lineWidth = 10;
    }
    if(isDrawing){
       this.ctx.moveTo(x1, y1);
       this.ctx.lineTo(x2, y2);
       this.ctx.stroke();
    }else{
      return ;
    }


    // return this.ctx.stroke();
  }


  // tslint:disable-next-line:typedef
  draw_line_ev(ev) {
    let rect = ev.target;
    let mousex = ev["clientX"] - rect["left"];
    let mousey = ev["clientY"] - rect["top"];
    // tslint:disable-next-line:no-conditional-assignment
    if (mousex < this.canvas.width || mousey < this.canvas.height) {

      this.draw_line(this.dctrl["prevx"], this.dctrl["prevy"], mousex, mousey,this.dctrl["drawing"] = true);
      this.dctrl["prevx"] = mousex;
      this.dctrl["prevy"] = mousey;
      // tslint:disable-next-line:align
    }
    // tslint:disable-next-line:align
    else {
    this.dctrl["drawing"] = false;

    mousex=null,
    mousey=null;
    this.draw_line(this.dctrl["prevx"], this.dctrl["prevy"], mousex, mousey,this.dctrl["drawing"] = false);
     this.dctrl["prevx"] = null;
      this.dctrl["prevy"] = null;
    }
  }
 ClearCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  EraseCanvas() {
    this.isErase = !this.isErase;
    this.socket.emit("erase", this.isErase);
  }

  Mousedown() {
    //
    this.mainService.hotListenerMousedown().subscribe((event) => {
      let eventObj = {
        offsetX: event["offsetX"],
        offsetY: event["offsetY"],
        clientX: event["clientX"],
        clientY: event["clientY"],

        target: event["target"].getBoundingClientRect(),
      };
      this.fMousedown(eventObj);
    });

    this.socket.on("hotListenerMousedown", (event) => {
      this.fMousedown(event);
    });
  }

  fMousedown(event: any) {
    if (event !== undefined) {
      this.dctrl["prevx"] = event["offsetX"];
      this.dctrl["prevy"] = event["offsetY"];
      let rect = event["target"];

      this.dctrl["drawing"] = true;

      this.dctrl["prevx"] = event["clientX"] - rect["left"];
      this.dctrl["prevy"] = event["clientY"] - rect["top"];
    }
  }
  MouseMove() {
    this.mainService.hotListenerMousemove().subscribe((event) => {
      let eventObj = {
        offsetX: event["offsetX"],
        offsetY: event["offsetY"],
        clientX: event["clientX"],
        clientY: event["clientY"],

        target: event["target"].getBoundingClientRect(),
      };
      this.fMouseMove(eventObj);
    });
    this.socket.on("hotListenerMousemove", (event) => {
      this.fMouseMove(event);
    });
  }

  fMouseMove(event: any) {
    if (this.dctrl["drawing"]) {
      this.draw_line_ev(event);

      this.dctrl["prevx"] = event["offsetX"];
      this.dctrl["prevy"] = event["offsetY"];
    }
  }
  MouseOut() {
    this.dctrl["drawing"] = false;

    this.socket.on("hotListenerMouseout", (event) => {
      this.dctrl["drawing"] = false;
    });
  }

  MouseUp() {
    this.mainService.hotListenerMouseup().subscribe((event) => {
      let eventObj = {
        offsetX: event["offsetX"],
        offsetY: event["offsetY"],
        clientX: event["clientX"],
        clientY: event["clientY"],

        target: event["target"].getBoundingClientRect(),
      };
      this.fMouseUp(eventObj);
    });
    //

    this.socket.on("hotListenerMouseup", (event) => {
      this.fMouseUp(event);
    });
  }

  fMouseUp(event: any) {
    if (this.dctrl["drawing"]) {
      this.draw_line_ev(event);

      this.dctrl["prevx"] = 0;
      this.dctrl["prevy"] = 0;
      this.dctrl["drawing"] = false;
    }
  }

  getData(element: any): Subject<any> {
    const interval = setInterval(() => {
      this.data.next(element);
    }, 500);
    // tslint:disable-next-line:no-unused-expression
    () => clearInterval(interval);
    return this.data;
  }

  ShowPaletteColorBox(event) {
    this.colorPaletteBoxVisible = true;
    // show the box where the mouse is (add the height of the box to y coord)
  }

  // change line weight
  ChangeLineWidth() {
    this.ctx.lineWidth += 1;
    this.socket.emit("lineWidth", this.ctx.lineWidth);
  }
  ChangeLineMinus() {
    if (this.ctx.lineWidth > 1) {
      this.ctx.lineWidth -= 1;
      this.socket.emit("lineWidth", this.ctx.lineWidth);
    } else {
      return;
    }
  }

  ChangeColor(colors) {
    console.log(colors);
    this.ctx.strokeStyle = colors;
    this.selectedColor = colors;
    this.colorPaletteBoxVisible = false;
    this.socket.emit("lineColor", colors);
  }

  changeColorOutside() {
    this.socket.on("lineColor", (colorLine) => {
      this.ctx.strokeStyle = colorLine;
      this.selectedColor = colorLine;
    });
  }
  changeLineOutside() {
    this.socket.on(
      "lineWidth",
      (lineWidth) => (this.ctx.lineWidth = lineWidth)
    );
  }
  loadImg(canvas: any, ctx: any) {
    this.imgInput.addEventListener("change", (e) => {
      console.log(e);
      if (e.target.files) {
        let imageFile = e.target.files[0];
        console.log(imageFile, typeof imageFile);
        this.socket.emit("Image", imageFile);
        ctx.globalCompositeOperation = "source-over"; //here we get the image file
        this.fLoadingIMg(imageFile, canvas, ctx);
        //
      }
    });
  }
  fLoadingIMg(imageFile: any, canvas: any, ctx: any) {
    const reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onloadend = (e) => {
      this.myImage = new Image(); // Creates image object
      this.myImage.src = e.target.result;
      // Assigns converted image to image object

      console.log("myImage", this.myImage);
      this.myImage.onload = (ev) => {
        console.log(ev);
        console.log(canvas);
        // Creates a contect object
        canvas["width"] = ev.path[0]["width"]; // Assigns image's width to canvas
        canvas["height"] = ev.path[0]["height"];

        // Assigns image's height to canvas
        this.withAndHeight(this.canvas);
        ctx.drawImage(this.myImage, 10, 10, 150, 150);

        this.init();
        // Draws the image on canvas
        //let imgData = this.canvas.toDataURL("image/jpeg"0); // Assigns image base64 string in jpeg format to a variable
      };
    };
  }
  Erasing() {
    this.socket.on("erase", (eraseObj) => {
      console.log(eraseObj);

      this.isErase = eraseObj;
      this.ctx.globalCompositeOperation = "source-over";
      this.ctx.lineWidth = 10;
    });
  }
  // tslint:disable-next-line:align
  // tslint:disable-next-line:no-unused-expression
  // tslint:disable-next-line:align
  loadingImg(canvas: any, ctx: any) {
    this.socket.on("Image", (Image) => {
      let image = new Blob([new Uint8Array(Image).buffer]);
      console.log(image);
      // this.fLoadingIMg(image, canvas, ctx);
    });
  }
  withAndHeight(canvas: any) {
    console.log(this.isTeacherClassBlackB);
    if (this.isTeacherClassBlackB === false) {
      /*  this.width=parseInt( '1170', 10 ) + 'px';
      this.height= parseInt( '580', 10 ) + 'px';*/
      canvas.style.width = "1170px";
      canvas.style.height = "600px";
      canvas.width = "1170";
      canvas.height = "600";
    } else {
      canvas.style.width = "900px";
      canvas.style.height = "500px";
      canvas.width = "900";
      canvas.height = "500";
    }
    return canvas;
  }
}
