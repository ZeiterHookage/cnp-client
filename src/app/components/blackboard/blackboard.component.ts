import { MainServicesService } from './../../services/main-services.service';
import { Socket } from "ngx-socket-io";
import { Component, OnInit, Input, AfterViewInit } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

import { fabric } from "fabric";
import { history } from "fabric-history";
// tslint:disable-next-line:no-namespace
@Component({
  selector: "app-blackboard",
  templateUrl: "./blackboard.component.html",
  styleUrls: ["./blackboard.component.css"],
})
export class BlackboardComponent implements OnInit, AfterViewInit {
  @Input() isTeacherClassBlackB: any;
  @Input() title: any;
  url: any;
  canvas: any;
  ctx: any;
  myImage: any;
  image: any;
  isDrawing: boolean;
  mySubject = new BehaviorSubject<boolean>(false);
  drawingSubject = this.mySubject.asObservable();
  lastPosX: number=null ;
  lastPosY: number = null ;
  roomData:any;
  //
  fabricObject: any;
  constructor(private socket: Socket, private mainService: MainServicesService) {}

  ngOnInit() {


    this.canvas = new fabric.Canvas("blackboards", {
      selection: true,
      renderOnAddRemove: true,
      includeDefaultValues: false,

    });
    this.ctx = this.canvas.getContext("2d");
    this.canvas.selectionFullyContained = false;
    this.canvas.preserveObjectStacking = true;
    this.canvas.stopContextMenu = true;
    this.canvas.set({ history });
    this.withAndHeight(this.canvas);

    this.asignatureTitle();

    this.fEvents();
   this.renderBackGround();
  }

  ngAfterViewInit() {}

  readyCanvas(event: any) {
    this.canvas.isDrawingMode = event;
  }
  asignatureTitle() {
    const title = new fabric.IText(this.title, {
      selectable: false,
      excludeFromExport: false,
      absolutePositioned:true,

    });

    this.canvas.add(title).centerObjectH(title).set({selectable:false});
  }

  ClearCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  fEvents() {


    this.canvas.on('path:created',  ()=> {

        const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    });

    this.canvas.on('selection:updated',  ()=> {

      const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    });


    this.canvas.on('mouse:down',  ()=> {

      const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    });

    this.canvas.on('mouse:up',  ()=> {

      const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    });

    this.canvas.on('editing:entered',  ()=> {

      const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    });

    this.canvas.on('editing:exited',  ()=> {

      const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    });
    this.canvas.on('selection:changed',  ()=> {

      const room = this.mainService.getTaRR();
      this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
    })

    this.canvas.on('object:rotating',  (e)=> {

      const room = this.mainService.getTaRR();
      if(e['target']['__proto__']['type'] === 'image'){
        this.canvas.off('object:rotating')
        this.canvas.on('object:rotated',()=>{
          this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
        })

      }else{
        this.canvas.on('object:rotating',()=>{
          this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
       })
      }
    })

    this.canvas.on('object:scaling',  (e)=> {
      const room = this.mainService.getTaRR();
      if(e['target']['__proto__']['type'] === 'image'){
        this.canvas.off('object:scaling')
        this.canvas.on('object:scaled',()=>{
          this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
        })

      }else{
        this.canvas.on('object:scaling',()=>{
          this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
       })
      }
    })
    this.canvas.on('object:moving',  (e)=> {

        const room = this.mainService.getTaRR();
        if(e['target']['__proto__']['type'] === 'image'){
          this.canvas.off('object:moving')
          this.canvas.on('object:moved',()=>{
            this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
          })

        }else{
          this.canvas.on('object:moving',()=>{
             this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
          })

        }


    });
    this.canvas.on('object:skewing',  (e)=> {

      const room = this.mainService.getTaRR();
      if(e['target']['__proto__']['type'] === 'image'){
        this.canvas.off('object:skewing')
        this.canvas.on('object:skewed',()=>{
          this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
        })

      }else{
        this.canvas.on('object:skewing',()=>{
          this.socket.emit('refresh',  {canvas:this.stringifyCanvas(), room:room.room})
       })
      }
    });
    this.canvas.on('object:added',  (options)=> {

      options.target.set('added',true);

       const room = this.mainService.getTaRR();
      this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
    });

    this.canvas.on('object:removed',  (options)=> {
       const room = this.mainService.getTaRR();
      this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
    });

    /*this.canvas.on('event:added',  (options)=> {
      const room = this.mainService.getTaRR();
     this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
   });

   this.canvas.on('event:selected',  (options)=> {
    const room = this.mainService.getTaRR();
   this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
 });

 this.canvas.on('event:deselected',  (options)=> {
  const room = this.mainService.getTaRR();
 this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
});

this.canvas.on('event:modified',  (options)=> {
  const room = this.mainService.getTaRR();
 this.socket.emit('refresh', {canvas:this.stringifyCanvas(), room:room.room})
});*/
  /* this.canvas.on('object:modified',  ()=> {

this.socket.emit('refresh', this.emitting())
      let obj = e.target;

                if (obj.getLeft() < 0
					|| obj.getTop() < 0
					|| obj.getLeft() + obj.getWidth() > this.canvas.getWidth()
					|| obj.getTop() + obj.getHeight() > this.canvas.getHeight()) {
                    if (obj.getAngle() != obj.originalState.angle) {
                        obj.setAngle(obj.originalState.angle);
                    }
                    else {
                        obj.setTop(obj.originalState.top);
                        obj.setLeft(obj.originalState.left);
                        obj.setScaleX(obj.originalState.scaleX);
                        obj.setScaleY(obj.originalState.scaleY);
                    }
                    obj.setCoords();
                }else{
                  return
                }


    });*/


  }



  withAndHeight(canvas: any) {
    /*fabric.Image.fromURL("../../../assets/img/boardFront.png", function (img) {
      img.set({
        scaleX: canvas.width / img.width,
        scaleY: canvas.height / img.height,
      });

      canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
        excludeFromExport: true,
      });
    });*/
    canvas.setDimensions(
      {
        width: 1170,
        height: 530,
        left: 17,
      },
      { backstoreOnly: false, cssOnly: false }
    );

    /*if (this.isTeacherClassBlackB === false) {
      fabric.Image.fromURL(
        "../../../assets/img/boardFront.png",
        function (img) {
          img.set({
            scaleX: canvas.width / img.width,
            scaleY: canvas.height / img.height,
          });

          canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
        }
      );
      canvas.setDimensions(
        {
          width: 1170,
          height: 530,
          left: 17,
        },
        { backstoreOnly: false, cssOnly: false }
      );
    } else {
      fabric.Image.fromURL(
        "../../../assets/img/boardFront.png",
        function (img) {
          img.set({
            scaleX: canvas.width / img.width,
            scaleY: canvas.height / img.height,
          });
          canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
        }
      );
      canvas.setDimensions(
        {
          width: 900,
          height: 500,
          left: 17,
        },
        { backstoreOnly: false, cssOnly: false }
      );
    }*/
  }

  stringifyCanvas () {
    return this.canvas.toJSON([
      'selectable','added','lockMovementX','lockMovementY','lockRotation','lockScalingFlip','lockScalingFlip',
    'lockScalingY','lockSkewingX','lockSkewingY',
  ]);
  }

  renderBackGround(){
    this.socket.on('refresh',(data)=>{
      console.log('data',data);
  this.renderCanvas(data);

    })
  }
  renderCanvas (data) {
this.roomData = this.mainService.getTaRR();

if(data.room === this.roomData['room']){

  data.canvas.objects.forEach(element => {
     if(element['added'])
      {
        this.canvas.off('object:added');
        this.canvas.loadFromJSON(data.canvas, this.canvas.renderAll.bind(this.canvas));

      }
      else{
        this.canvas.loadFromJSON(data.canvas, this.canvas.renderAll.bind(this.canvas));
      }

});
}


      }
}
