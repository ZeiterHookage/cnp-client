import { NgForm } from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  Renderer2,
  Inject,
} from "@angular/core";
import { Subject } from "rxjs";
import { CdkDragMove } from "@angular/cdk/drag-drop";
import { DOCUMENT } from "@angular/common";

@Component({
  selector: "app-gradients",
  templateUrl: "./gradients.component.html",
  styleUrls: ["./gradients.component.css"],
})
export class GradientsComponent implements OnInit, AfterViewInit {
  @ViewChild("gradx_panel", { static: false }) gradx: any;
  inputGradP: any;
  isAddButton = false;
  isRemoveButton = false;
  tinyCounter = 0;
  id: string;
  inputGrad = [];
  obs$ = new Subject<any>();
  myObs = this.obs$.asObservable();
  gradPanel: any;
  colorOrder = [];
  percent: any;
  value = "#000000";
  codeBlock: any;
  inputBox: any;
  gradient: any;
  background: any;
  rule: any;
  inputBoxId: any;
  GradType: any;
  GradHdirection: any;
  GradVdirection: any;
  constructor(
    private sanitizer: DomSanitizer,
    @Inject(DOCUMENT) private documents: Document,
    private renderer: Renderer2
  ) {
    this.id = `sliders${this.tinyCounter}`;
    this.inputGrad.push(this.id);
  }

  ngOnInit(): void {
    this.gradPanel = document.getElementById("gradx_panel");
    this.gradientMain();
  }

  ngAfterViewInit() {
    this.codeBlock = document.querySelector(".cp-info");
    this.updateCode();
  }

  dragMoved($event: CdkDragMove) {
    //console.log($event.source.data.id, $event.source.data.value);
    this.inputBox = document.querySelector(`#${$event.source.data.id}`);
    this.inputBox.addEventListener("click", (event) => {
      this.inputBoxId = event["srcElement"]["id"];
    });
    this.percent = ($event.source.getFreeDragPosition().x / 274) * 100;

    switch ($event.source.data.id) {
      case "sliders0":
        this.gradPanel.style.setProperty(
          "--firstside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();

        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders0" && isRemove === "sliders0") {
            this.gradPanel.style.setProperty("--firstside", this.value);
            this.gradPanel.style.setProperty("--grad-hint", "0%");
            this.updateCode();
          } else {
            return;
          }
        });

        break;

      case "sliders1":
        this.gradPanel.style.setProperty(
          "--secondside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint2", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders1" && isRemove === "sliders1") {
            this.gradPanel.style.setProperty("--secondside", this.value);
            this.gradPanel.style.setProperty("--grad-hint2", "0%");
            this.updateCode();
          } else {
            return;
          }
        });

        break;

      case "sliders2":
        this.gradPanel.style.setProperty(
          "--thirdside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint3", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders2" && isRemove === "sliders2") {
            this.gradPanel.style.setProperty("--thirdside", this.value);
            this.gradPanel.style.setProperty("--grad-hint3", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;

      case "sliders3":
        this.gradPanel.style.setProperty(
          "--fourthside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint4", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders3" && isRemove === "sliders3") {
            this.gradPanel.style.setProperty("--fourthside", this.value);
            this.gradPanel.style.setProperty("--grad-hint4", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;

      case "sliders4":
        this.gradPanel.style.setProperty(
          "--fifthside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint5", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;

        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders4" && isRemove === "sliders4") {
            this.gradPanel.style.setProperty("--fifthside", this.value);
            this.gradPanel.style.setProperty("--grad-hint5", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;

      case "sliders5":
        this.gradPanel.style.setProperty(
          "--sixtxhside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint6", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders5" && isRemove === "sliders5") {
            this.gradPanel.style.setProperty("--sixtxhside", this.value);
            this.gradPanel.style.setProperty("--grad-hint6", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;
      case "sliders6":
        this.gradPanel.style.setProperty("--seventh", $event.source.data.value);
        this.gradPanel.style.setProperty("--grad-hint7", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders6" && isRemove === "sliders6") {
            this.gradPanel.style.setProperty("--seventh", this.value);
            this.gradPanel.style.setProperty("--grad-hint7", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;

      case "sliders7":
        this.gradPanel.style.setProperty("--eight", $event.source.data.value);
        this.gradPanel.style.setProperty("--grad-hint8", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders7" && isRemove === "sliders7") {
            this.gradPanel.style.setProperty("--eight", this.value);
            this.gradPanel.style.setProperty("--grad-hint8", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;

      case "sliders8":
        this.gradPanel.style.setProperty("--ninth", $event.source.data.value);
        this.gradPanel.style.setProperty("--grad-hint9", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders8" && isRemove === "sliders8") {
            this.gradPanel.style.setProperty("--ninth", this.value);
            this.gradPanel.style.setProperty("--grad-hint9", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;

      default:
        // this.sanitaizer.bypassSecurityTrustStyle("--thirdside");
        this.gradPanel.style.setProperty(
          "--tenthside",
          $event.source.data.value
        );
        this.gradPanel.style.setProperty("--grad-hint10", `${this.percent}%`);
        this.inputBox.style.background = $event.source.data.value;
        this.updateCode();
        this.myObs.subscribe((isRemove) => {
          if (this.inputBox.id === "sliders9" && isRemove === "sliders9") {
            this.gradPanel.style.setProperty("--tenthside", this.value);
            this.gradPanel.style.setProperty("--grad-hint10", "0%");
            this.updateCode();
          } else {
            return;
          }
        });
        break;
    }
  }

  addButton() {
    if (this.tinyCounter <= 10) {
      ++this.tinyCounter;
      this.id = `sliders${this.tinyCounter}`;
      this.inputGrad.push(this.id);
    } else {
      this.tinyCounter = 10;
      this.isAddButton = false;
    }
  }
  removeButton() {
    if (this.inputGrad.length < 0 && this.tinyCounter < 0) {
      this.tinyCounter = 0;

      return;
    } else {
      this.obs$.next(this.inputBoxId);
      this.inputGrad = this.inputGrad.filter(
        (element) => element != this.inputBoxId
      );

      --this.tinyCounter;
      this.updateCode();
    }
  }
  Hdirection(event: any) {
    this.obs$.next(event);
  }
  Vdirection(event: any) {
    this.obs$.next(event);
  }
  gradType(event: any) {
    this.obs$.next(event);
  }
  gradientMain() {
    this.myObs.subscribe((x) => {
      if (
        x === "linear-gradient" ||
        x === "radial-gradient" ||
        x === "radial-gradient-eclipse"
      ) {
        this.GradType = x;
      } else if (x === "left" || x === "right" || x === "center") {
        this.GradHdirection = x;
      } else {
        this.GradVdirection = x;
      }
      console.log(this.GradType, this.GradHdirection, this.GradVdirection);
      /*if (
        this.GradType !== undefined ||
        this.GradHdirection !== undefined ||
        this.GradVdirection !== undefined
      ) {
        switch (this.GradType) {
          case "radial-gradient":
            console.log("radial-gradient");
            break;

          case "radial-gradient-eclipse":
            console.log("radial-gradient-eclipse");
            break;

          default:
            console.log("linear-gradient", `${this.GradType}`);
            this.gradient = `${this.GradType}(to ${this.GradHdirection},var( --firstside) var(--grad-hint),
      var(--secondside) var( --grad-hint2),
      var(--thirdside) var(--grad-hint3),
      var(--fourthside) var(--grad-hint4),
      var(--fifthside) var(--grad-hint5),
      var(--sixtxhside) var(--grad-hint6),
      var(--seventh) var(--grad-hint7),
      var(--eight) var(--grad-hint8),
      var(--ninth) var(--grad-hint9),
      var(--tenthside) var(--grad-hint10)`;
            this.background = `{background:"${this.gradient}}`;
            console.log(this.background);
            const styles = this.documents.createElement(
              "STYLE"
            ) as HTMLStyleElement;

            styles.id = "gradx_panel";
            styles.innerHTML = `"background": ${this.gradient}`;
            // console.log(styles);
            this.renderer.appendChild(this.documents.head, styles);
            this.modifyStyles(".gradx_panel", { background: "blue" });

            this.renderer.appendChild(this.documents.head, styles);
            let background = `("background": ${this.gradient})`;
            this.renderer.setStyle(this.gradPanel, "background", this.gradient);
            break;
        }
      }*/
    });
  }
  updateCode() {
    const body = window.getComputedStyle(this.gradPanel);
    this.codeBlock["innerText"] = `background: ${this.GradType} to right:
  ${body.getPropertyValue("--firstside")} ${body.getPropertyValue(
      "--grad-hint"
    )}, ${body.getPropertyValue("--secondside")} ${body.getPropertyValue(
      "--grad-hint2"
    )},${body.getPropertyValue("--thirdside")} ${body.getPropertyValue(
      "--grad-hint3"
    )},
  ${body.getPropertyValue("--fourthside")} ${body.getPropertyValue(
      "--grad-hint4"
    )},${body.getPropertyValue("--fifthside")} ${body.getPropertyValue(
      "--grad-hint5"
    )},${body.getPropertyValue("--sixtxhside")} ${body.getPropertyValue(
      "--grad-hint6"
    )},
  ${body.getPropertyValue("--seventh")} ${body.getPropertyValue(
      "--grad-hint7"
    )},${body.getPropertyValue("--eight")} ${body.getPropertyValue(
      "--grad-hint8"
    )},${body.getPropertyValue("--eight")} ${body.getPropertyValue(
      "--grad-hint9"
    )},
  ${body.getPropertyValue("--tenthside")} ${body.getPropertyValue(
      "--grad-hint10"
    )}`;
  }

  modifyStyles(selector: string, styles: any) {
    const rulesToUpdate = this.findRules(new RegExp(`${selector}`, "g"));
    for (let rule of rulesToUpdate) {
      for (let key in styles) {
        rule.style[key] = styles[key];
      }
    }
  }
  /**
   * Finds all style rules that match the regular expression
   */
  private findRules(re: RegExp) {
    let foundRules: CSSStyleRule[] = [];

    let ruleListToCheck = Array.prototype.slice.call(
      this.documents.styleSheets
    );
    let sheet = [];
    // console.log(ruleListToCheck);

    /*ruleListToCheck.forEach((sheets) => {

sheet.push(sheets)



  /*for (let rule of <any[]>(sheet.cssRules || sheet.rules || [])) {
    console.log(rule);
  }

});*/

    for (let sheets of ruleListToCheck) {
      //console.log(sheets);
      sheet.push(sheets);
      /*for(let rule of <any[]>(sheets['cssRules'] || sheets['rules'] || [])){
    console.log(rule['selectorText']);*/
    }
    //console.log(sheet[0]['rules'])
    /* console.log("sheet12=>", sheet[12]["rules"][0]["style"]["cssText"]);
    console.log("sheet12=>", sheet[12]["rules"][1]["style"]);
    console.log("sheet12=>", sheet[12]["rules"][2]["style"]);
    console.log("sheet12=>", sheet[12]["rules"][3]["style"]);*/
    if (sheet[12] instanceof CSSStyleDeclaration) {
      //console.log("good", sheet[12]);
    } else if (sheet[12] instanceof CSSMediaRule) {
      //console.log("good too", sheet[12]);
    } else if (sheet[12] instanceof CSSStyleRule) {
      // console.log("good too2", sheet[12]);
    } else {
      // console.log("bad", sheet);
    }
    for (let rule of <any[]>(sheet["cssRules"] || sheet["rules"] || [])) {
      // console.log(rule);
    }
    /*for (let sheet of ruleListToCheck) {
      console.log(sheet);
      for (let rule of <any[]>(sheet.cssRules || sheet.rules || [])) {
          //console.log(rule['selectorText'])
        if (rule instanceof CSSStyleRule) {

          if (re.test(rule["selectorText"])) {
           // console.log('macht');
            //foundRules.push(rule);
          }
        } else if (rule instanceof CSSMediaRule) {
          // ruleListToCheck.push(rule);
        }
      }
    }*/

    return foundRules;
  }
}
