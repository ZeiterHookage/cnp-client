"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ChatScreenComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var ChatScreenComponent = /** @class */ (function () {
    function ChatScreenComponent(mainService, socket) {
        this.mainService = mainService;
        this.socket = socket;
        this.isChatScreen = false;
        this.formEnviar = document.getElementById("formEnviar");
        this.data = new core_1.EventEmitter();
        this.messages = [];
        this.textting = [];
        this.subscription = new rxjs_1.Subscription();
    }
    ChatScreenComponent.prototype.ngOnInit = function () {
        this.user = {
            name: this.mainService.getTaRR()["name"],
            room: this.mainService.getTaRR()["room"],
            id: this.mainService.getTaRR()["id"],
            img: this.mainService.getTaRR()['img']
        };
        console.log(this.user);
        this.mainService.chatConnection(this.user, this.messages, this.data);
    };
    ChatScreenComponent.prototype.ngAfterViewInit = function () { };
    ChatScreenComponent.prototype.ngAfterContentChecked = function () {
        this.divUsuarios = document.getElementById("divChatbox");
    };
    ChatScreenComponent.prototype.ngAfterViewChecked = function () {
        this.getMessages();
    };
    ChatScreenComponent.prototype.chatMessage = function (forma) {
        var _this = this;
        console.log(forma.value, this.user, this.user["name"], this.user["room"]);
        this.socket.emit("createMessage", {
            name: this.user["name"],
            room: this.user["room"],
            id: this.user["id"],
            img: this.user['img'],
            message: forma.value
        }, function (message) {
            _this.textting.push(message);
        });
    };
    ChatScreenComponent.prototype.mesageClass = function (event) {
        this.mesage = event[0];
    };
    ChatScreenComponent.prototype.appendMessage = function () {
        if (this.mesage !== undefined) {
            var newMessage = this.mesage.cloneNode(true);
            this.divUsuarios.appendChild(newMessage);
        }
    };
    ChatScreenComponent.prototype.scrollToBottom = function () {
        this.divUsuarios.scrollTop = this.divUsuarios.scrollHeight;
    };
    ChatScreenComponent.prototype.chatConnection = function (user) {
        var _this = this;
        if (user !== undefined) {
            this.socket.emit("enterToRoom", this.user, function (resp) {
                console.log('enterToRoom ==>', resp);
                return _this.data.emit(resp);
            });
            this.socket.on("privateMessage", function (message) {
                console.log("private Message:", message);
            });
        }
        // Escuchar información del servidor
        this.socket.on("createMessage", function (message) {
            _this.messages.push(message);
        });
        //when someone left the Room
        this.socket.on("peopleList", function (people) {
            console.log('peopleList ==>', people);
            return _this.data.emit(people);
        });
    };
    ChatScreenComponent.prototype.getMessages = function () {
        this.shouldScroll =
            this.divUsuarios.scrollTop + this.divUsuarios.clientHeight ===
                this.divUsuarios.scrollHeight;
        this.appendMessage();
        if (!this.shouldScroll) {
            this.scrollToBottom();
        }
    };
    ChatScreenComponent.prototype.ngOnDestroy = function () {
        this.isChatScreen = false;
        this.user = {};
        this.messages = [];
        this.textting = [];
        this.subscription.add(this.data.subscribe());
        this.subscription.unsubscribe();
    };
    __decorate([
        core_1.Output()
    ], ChatScreenComponent.prototype, "data");
    __decorate([
        core_1.Input()
    ], ChatScreenComponent.prototype, "isTeacherClass");
    ChatScreenComponent = __decorate([
        core_1.Component({
            selector: "app-chat-screen",
            templateUrl: "./chat-screen.component.html",
            styleUrls: ["./chat-screen.component.css"]
        })
    ], ChatScreenComponent);
    return ChatScreenComponent;
}());
exports.ChatScreenComponent = ChatScreenComponent;
