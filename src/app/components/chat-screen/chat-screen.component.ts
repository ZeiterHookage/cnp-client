import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  AfterViewInit,
  AfterContentChecked,
  AfterViewChecked,
} from "@angular/core";
import { MainServicesService } from "../../services/main-services.service";
import { NgForm } from "@angular/forms";
import { Subscription } from "rxjs";
import { Socket } from "ngx-socket-io";

@Component({
  selector: "app-chat-screen",
  templateUrl: "./chat-screen.component.html",
  styleUrls: ["./chat-screen.component.css"],
})
export class ChatScreenComponent
  implements
    OnInit,
    AfterViewInit,
    AfterContentChecked,
    OnDestroy,
    AfterViewChecked {
  isChatScreen: boolean = false;
  user: object;
  formEnviar = document.getElementById("formEnviar");
  @Output() data: EventEmitter<any> = new EventEmitter();
  @Input() isTeacherClass: boolean;
  messages: any = [];
  textting: any = [];

  subscription: Subscription = new Subscription();
  divUsuarios: any;
  mesage: any;
  shouldScroll: any;
  constructor(
    private mainService: MainServicesService,
    private socket: Socket
  ) {}

  ngOnInit() {
    this.user = {
      name: this.mainService.getTaRR()["name"],
      room: this.mainService.getTaRR()["room"],
      id: this.mainService.getTaRR()["id"],
      img: this.mainService.getTaRR()['img']
    };

    console.log(this.user);
    this.mainService.chatConnection(this.user,this.messages, this.data);

  }
  ngAfterViewInit() {}
  ngAfterContentChecked() {
    this.divUsuarios = document.getElementById("divChatbox");
  }
  ngAfterViewChecked() {
    this.getMessages();
  }
  chatMessage(forma: NgForm) {
    console.log(forma.value, this.user, this.user["name"], this.user["room"]);
    this.socket.emit(
      "createMessage",
      {
        name: this.user["name"],
        room: this.user["room"],
        id:this.user["id"],
        img: this.user['img'],
        message: forma.value,
      },
      (message) => {
        this.textting.push(message);
      }
    );
  }

  mesageClass(event: HTMLElement) {
    this.mesage = event[0];
  }
  appendMessage() {
    if (this.mesage !== undefined) {
      const newMessage = this.mesage.cloneNode(true);
      this.divUsuarios.appendChild(newMessage);
    }
  }

  scrollToBottom() {
    this.divUsuarios.scrollTop = this.divUsuarios.scrollHeight;
  }



  chatConnection(user: any) {

    if (user !== undefined) {
      this.socket.emit("enterToRoom", this.user, (resp) => {
          console.log('enterToRoom ==>',resp);
        return this.data.emit(resp);
      });


      this.socket.on("privateMessage", (message: any) => {
        console.log("private Message:", message);
      });
    }
    // Escuchar información del servidor
    this.socket.on("createMessage", (message) => {

      this.messages.push(message);
    });


    //when someone left the Room
    this.socket.on("peopleList", (people) => {
      console.log('peopleList ==>',people);
      return this.data.emit(people);
    });

  }



  getMessages() {

    this.shouldScroll =
      this.divUsuarios.scrollTop + this.divUsuarios.clientHeight ===
      this.divUsuarios.scrollHeight;

    this.appendMessage();
    if (!this.shouldScroll) {
      this.scrollToBottom();
    }
  }





  ngOnDestroy() {
    this.isChatScreen = false;
    this.user = {};
    this.messages = [];
    this.textting = [];
    this.subscription.add(this.data.subscribe());
    this.subscription.unsubscribe();
  }
}
