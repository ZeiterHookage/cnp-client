"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.SchoolCamera = void 0;
var io = require("socket.io-client");
var environment_1 = require("src/environments/environment");
var SchoolCamera = /** @class */ (function () {
    function SchoolCamera(chatEvt, localVideo, remoteVideo) {
        this.uri = environment_1.environment.wsUrl;
        this.socket = io(this.uri);
        navigator["getUserMedia"] =
            navigator["getUserMedia"] ||
                navigator["webkitGetUserMedia"] ||
                navigator["webkitGetUserMedia"];
        this.iceServers = {
            iceServer: [
                { urls: "stun:stun.services.mozilla.com" },
                { urls: "stun:stun.l.google.com:19302" },
            ]
        };
        this.streamConstrains = {
            audio: true,
            video: true
        };
        this.socketFn(chatEvt, localVideo, remoteVideo);
    }
    SchoolCamera.prototype.socketFn = function (chatEvt, localVideo, remoteVideo) {
        var _this = this;
        this.rtcPeerConnection = new RTCPeerConnection(this.iceServers);
        this.remoteVideos = remoteVideo;
        console.log(chatEvt, localVideo, this.remoteVideos);
        this.socket.emit("create or join", chatEvt["room"]);
        this.socket.on("created", function (room) {
            navigator.mediaDevices
                .getUserMedia(_this.streamConstrains)
                .then(function (stream) {
                _this.localStream = stream;
                localVideo.srcObject = stream;
                _this.isCaller = true;
            })["catch"](function (error) {
                return console.log("An error ocurred when accessing media devices", error);
            });
        });
        this.socket.on("joined", function (room) {
            navigator.mediaDevices.getUserMedia(_this.streamConstrains)
                .then(function (stream) {
                _this.localStream = stream;
                localVideo.srcObject = stream;
                _this.socket.emit("ready", chatEvt["room"]);
            })["catch"](function (error) {
                return console.log("An error ocurred when accessing media devices", error);
            });
        });
        this.socket.on("candidate", function (event) {
            console.log("event", event);
            var candidate = new RTCIceCandidate({
                sdpMLineIndex: event.type,
                candidate: event.candidate
            });
            _this.rtcPeerConnection.addIceCandidate(candidate);
            console.log("recived candidate", candidate);
        });
        this.socket.on("ready", function () {
            if (_this.isCaller) {
                console.log("rtcPeerConnection", _this.rtcPeerConnection);
                _this.rtcPeerConnection.onicecandidate = _this.onIceCandidate;
                _this.rtcPeerConnection.ontrack = _this.onAddStream;
                _this.localStream.getTracks().forEach(function (track) {
                    _this.rtcPeerConnection.addTrack(track, _this.localStream);
                });
                _this.rtcPeerConnection
                    .createOffer()
                    .then(function (sessionDescription) {
                    console.log("sendind offer", sessionDescription);
                    _this.rtcPeerConnection.setLocalDescription(sessionDescription);
                    _this.socket.emit("offer", {
                        type: "offer",
                        sdp: sessionDescription,
                        room: chatEvt["room"]
                    });
                })["catch"](function (error) {
                    return console.log("An error ocurred when accessing media devices", error);
                });
                // this.dataChannel = this.rtcPeerConnection.createDataChannel(chatEvt['room']);
                //this.dataChannel.onmessage = event => h2CallName.innerText = event.data;
            }
        });
        this.socket.on("offer", function (event) {
            if (!_this.isCaller) {
                _this.rtcPeerConnection.onicecandidate = _this.onIceCandidate;
                _this.rtcPeerConnection.ontrack = _this.onAddStream;
                _this.localStream.getTracks().forEach(function (track) {
                    _this.rtcPeerConnection.addTrack(track, _this.localStream);
                });
                console.log("recived offer", event);
                _this.rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event.sdp));
                _this.rtcPeerConnection
                    .createAnswer()
                    .then(function (sessionDescription) {
                    console.log("sending answer", sessionDescription);
                    _this.rtcPeerConnection.setLocalDescription(sessionDescription);
                    _this.socket.emit("answer", {
                        type: "answer",
                        sdp: sessionDescription,
                        room: chatEvt["room"]
                    });
                })["catch"](function (err) { return console.log(err); });
                /* this.rtcPeerConnection.ondatachannel = event => {
                    console.log(event);
                    this.dataChannel = event.channel;
                   // this.dataChannel.onmessage = event => h2CallName.innerText = event.data;
        
                }*/
            }
        });
        this.socket.on("answer", function (event) {
            console.log("recived answer", event);
            _this.rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event.sdp));
        });
    };
    SchoolCamera.prototype.onAddStream = function (event) {
        this.remoteVideos.srcObject = event.streams[0];
        this.remoteStream = event.streams[0];
    };
    //error
    SchoolCamera.prototype.onIceCandidate = function (event) {
        console.log("onIceCandidate", event);
        if (event.candidate) {
            console.log("sending ice candidate", event.candidate);
            this.socket.emit("candidate", __assign({}, event.cadidate));
        }
    };
    return SchoolCamera;
}());
exports.SchoolCamera = SchoolCamera;
