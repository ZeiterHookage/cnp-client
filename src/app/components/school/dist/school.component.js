"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SchoolComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var SchoolComponent = /** @class */ (function () {
    function SchoolComponent(mainService, socket) {
        this.mainService = mainService;
        this.socket = socket;
        // schoolCamera: SchoolCamera;
        this.chatEvents = {}; //take are the users connected
        this.chatMessage = ([]);
        this.ObsSubc = new rxjs_1.Subscription();
        this.chat$ = new rxjs_1.BehaviorSubject(false);
        this.chatObs = this.chat$.asObservable();
        this.bboard$ = new rxjs_1.BehaviorSubject(false);
        this.bboardObs = this.bboard$.asObservable();
        this.cam$ = new rxjs_1.BehaviorSubject(false);
        this.camObs = this.cam$.asObservable();
        this.subscription = new rxjs_1.Subscription();
    }
    // tslint:disable-next-line:typedef
    SchoolComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.roomData = this.mainService.getTaRR();
        // emit an event to all connected sockets
        this.onCamera();
        this.onBlackboard();
        this.onChat();
        this.socket.on("bBTitle", function (title) { return (_this.title = title); });
    };
    SchoolComponent.prototype.chatEvent = function (e) {
        var _this = this;
        console.log(e);
        this.Obs$ = new rxjs_1.Observable(function (observer) {
            observer.next(e);
        });
        this.ObsSubc = this.Obs$.subscribe(function (user) { return (_this.chatEvents = user); });
    };
    SchoolComponent.prototype.onCamera = function () {
        var _this = this;
        this.socket.on("isCamera", function (isCamera) {
            _this.cam$.next(isCamera);
            _this.camObs.subscribe(function (camera) {
                console.log(camera);
                if (camera["room"] === _this.roomData["room"]) {
                    _this.isCamera = camera["camera"];
                }
                else {
                    return;
                }
            });
        });
    };
    /*getDataChat() {
    if (!this.myObservableArray) {
  
    this.myObservableArray = this.getData();
    this.subscription.add(this.myObservableArray.subscribe());
  
    }
  }*/
    SchoolComponent.prototype.onChat = function () {
        var _this = this;
        this.socket.on("isChat", function (isChat) {
            console.log("isChat", isChat);
            _this.chat$.next(isChat);
            _this.chatObs.subscribe(function (chat) {
                console.log(chat);
                if (chat["room"] === _this.roomData["room"]) {
                    _this.isChatting = chat["chat"];
                }
                else {
                    return;
                }
            });
        });
    };
    SchoolComponent.prototype.onBlackboard = function () {
        var _this = this;
        this.socket.on("isBlackboard", function (isBlackboard) {
            _this.bboard$.next(isBlackboard);
            _this.bboardObs.subscribe(function (bb) {
                console.log(bb);
                if (bb["room"] === _this.roomData["room"]) {
                    _this.isBlackboard = bb["bb"];
                }
                else {
                    return;
                }
            });
        });
    };
    SchoolComponent.prototype.ngOnDestroy = function () {
        this.ObsSubc.unsubscribe();
        this.chatEvents = {};
    };
    SchoolComponent = __decorate([
        core_1.Component({
            selector: "app-school",
            templateUrl: "./school.component.html",
            styleUrls: ["./school.component.css"],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        })
    ], SchoolComponent);
    return SchoolComponent;
}());
exports.SchoolComponent = SchoolComponent;
