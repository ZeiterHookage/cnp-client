import {
  Component,
  OnDestroy,
  OnInit,
  ChangeDetectionStrategy,
} from "@angular/core";
import { MainServicesService } from "src/app/services/main-services.service";

import { BehaviorSubject, Observable, Subject, Subscription } from "rxjs";
import { Socket } from "ngx-socket-io";
import { filter } from "rxjs/operators";

@Component({
  selector: "app-school",
  templateUrl: "./school.component.html",
  styleUrls: ["./school.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SchoolComponent implements OnInit, OnDestroy {
  // schoolCamera: SchoolCamera;
  chatEvents: any = {}; //take are the users connected
  chatMessage: any = ([] = []);
  Obs$: Observable<any>;
  ObsSubc: Subscription = new Subscription();
  chat$ = new BehaviorSubject<boolean>(false);
  chatObs = this.chat$.asObservable();
  bboard$ = new BehaviorSubject<boolean>(false);
  bboardObs = this.bboard$.asObservable();
  cam$ = new BehaviorSubject<boolean>(false);
  camObs = this.cam$.asObservable();
  localVideo: any;
  remoteVideo: any;
  isCamera: boolean;
  isChatting: boolean;
  isBlackboard: boolean;
  myObservableArray: Observable<any>;
  title: any;
  subscription: Subscription = new Subscription();

  hotListenerMousedown: any;
  hotListenerMousemove: any;
  hotListenerMouseout: any;
  hotListenerMouseup: any;
  roomData: any;
  constructor(
    private mainService: MainServicesService,
    private socket: Socket
  ) {}

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.roomData = this.mainService.getTaRR();
    // emit an event to all connected sockets
    this.onCamera();
    this.onBlackboard();
    this.onChat();
    this.socket.on("bBTitle", (title) => (this.title = title));
  }

  chatEvent(e: Event) {
    console.log(e);
    this.Obs$ = new Observable((observer) => {
      observer.next(e);
    });
    this.ObsSubc = this.Obs$.subscribe((user) => (this.chatEvents = user));
  }
  onCamera() {
    this.socket.on("isCamera", (isCamera) => {
      this.cam$.next(isCamera);
      this.camObs.subscribe((camera) => {
        console.log(camera);
        if (camera["room"] === this.roomData["room"]) {
          this.isCamera = camera["camera"];
        } else {
          return;
        }
      });
    });
  }
  /*getDataChat() {
  if (!this.myObservableArray) {

  this.myObservableArray = this.getData();
  this.subscription.add(this.myObservableArray.subscribe());

  }
}*/

  onChat() {
    this.socket.on("isChat", (isChat) => {
      console.log("isChat", isChat);
      this.chat$.next(isChat);

      this.chatObs.subscribe((chat) => {
        console.log(chat);
        if (chat["room"] === this.roomData["room"]) {
          this.isChatting = chat["chat"];
        } else {
          return;
        }
      });
    });
  }

  onBlackboard() {
    this.socket.on("isBlackboard", (isBlackboard) => {
      this.bboard$.next(isBlackboard);

      this.bboardObs.subscribe((bb) => {
        console.log(bb);
        if (bb["room"] === this.roomData["room"]) {
          this.isBlackboard = bb["bb"];
        } else {
          return;
        }
      });
    });
  }

  ngOnDestroy() {
    this.ObsSubc.unsubscribe();
    this.chatEvents = {};
  }
}
