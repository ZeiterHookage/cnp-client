import {
  Component,
  Input,
  OnInit,
  ViewChild,
  AfterViewInit,
} from "@angular/core";
import { fabric } from "fabric";
import { Socket } from "ngx-socket-io";

import { range, Observable, from } from "rxjs";
import { filter} from "rxjs/operators";


@Component({
  selector: "app-blackboard-toolbar",
  templateUrl: "./blackboard-toolbar.component.html",
  styleUrls: ["./blackboard-toolbar.component.css"],
})
export class BlackboardToolbarComponent implements OnInit, AfterViewInit {
  @Input() Tcanvas: any;
  @Input() Tctx: any;

  _clipboard: any;
  fontSize: any = [];
  StrokeLine:any=[];
  fFamilys: any;
  activeObject: fabric.Object;
  isStrokeColor=false;
  isFowardButtom = false;
  isFrontButtom = false;
  isBackwardsButtom = false;
  isBackButtom = false;
  isBoldButtom = false;
  isItalicButtom = false;
  isUnderlineButtom = false;
  isStrikethroughButtom = false;
  isAlignLeftButtom = false;
  isAlignCenterButtom = false;
  isAlignRightButtom = false;
  isAlignJustify = false;
  isCopyButtom = false;
  isPasteButtom = false;
  isUndoButtom = false;
  isRedoButtom = false;
  isGroupsButtom = false;
  isUngroupsButtom = false;
  isGradientButtom = false;
  isLockButtom = false;
  isSelectAllButtom = false;
  isUnselectAllButtom = false;
  isTrashButtom=false;
  inputGroupSelect01:any;
  fontFamily = [
    "arial",
    "helvetica",
    "myriad pro",
    "delicious",
    "verdana",
    "georgia",
    "courier",
    "comic sans ms",
    "impact",
    "monaco",
    "optima",
    "hoefler text",
    "plaster",
    "engagement",
  ];
  constructor( private socket: Socket) {}

  ngOnInit(): void {
    range(8, 65).subscribe((fsize) => this.fontSize.push(fsize));
    range(1,10).subscribe((Sl)=>{this. StrokeLine.push(Sl)});
    this.inputGroupSelect01= document.getElementsByClassName('stroke');


  }
  ngAfterViewInit() {}

  boldButtom() {
    this.isBoldButtom = !this.isBoldButtom;

    if (this.isBoldButtom) {
      this.Tcanvas.getActiveObject().setSelectionStyles({ fontWeight: "bold" }).setCoords();
       this.Tcanvas.renderAll();

    } else {

      this.Tcanvas.getActiveObject().setSelectionStyles({ fontWeight: "normal" }).setCoords();
      this.Tcanvas.renderAll()
    }
  }

  italicButtom() {
    this.isItalicButtom = !this.isItalicButtom;
    if (this.isItalicButtom) {
      this.Tcanvas.getActiveObject()
        .setSelectionStyles({ fontStyle: "italic" })
        .setCoords();
            this.Tcanvas.renderAll()
    } else {
      this.Tcanvas.getActiveObject().setSelectionStyles({ fontStyle: "normal" }).setCoords();
      this.Tcanvas.renderAll()
    }
  }
  underlineButtom() {
    this.isUnderlineButtom = !this.isUnderlineButtom;

    if (this.isUnderlineButtom) {
      this.Tcanvas.getActiveObject()
        .setSelectionStyles({ underline: true })
        .setCoords();
      this.Tcanvas.renderAll();
    } else {
      this.Tcanvas.getActiveObject()
      .setSelectionStyles({ underline: false })
      .setCoords();
      this.Tcanvas.renderAll()
    }
  }
  strikethroughButtom() {
    this.isStrikethroughButtom = !this.isStrikethroughButtom;

    if (this.isStrikethroughButtom) {
      this.Tcanvas.getActiveObject()
        .setSelectionStyles({ linethrough: true })
        .setCoords();
      this.Tcanvas.renderAll();
    } else {
      this.Tcanvas.getActiveObject()
      .setSelectionStyles({ linethrough: false })
      .setCoords();
      this.Tcanvas.renderAll();
    }
  }

  fTextCommon() {
    this.Tcanvas.getActiveObject().set("fontWeight", "normal");

    this.Tcanvas.getActiveObject().set("fontStyle", "");

    this.Tcanvas.getActiveObject().set("textDecoration", "");

    this.Tcanvas.getActiveObject().set("textDecoration", "");

    this.Tcanvas.getActiveObject().set("textDecoration", "");

    this.Tcanvas.getActiveObject().set("fontStyle", "normal");
  }

  alignLeftButtom() {

      this.isAlignCenterButtom = false;
      this.isAlignRightButtom = false;
      this.isAlignJustify = false;
      this.Tcanvas.getActiveObject().set({ textAlign: "left" }).setCoords();
      this.Tcanvas.renderAll();

  }

  alignCenterButtom() {

      this.isAlignLeftButtom = false;
      this.isAlignRightButtom = false;
      this.isAlignJustify = false;
      this.Tcanvas.getActiveObject().set({ textAlign: "center" }).setCoords();
      // this.Tcanvas.getActiveObject().setSelectionStyles({textAlign:'Center'})
      this.Tcanvas.renderAll();

  }

  alignRightButtom() {

      this.isAlignLeftButtom = false;
      this.isAlignCenterButtom = false;
      this.isAlignJustify = false;
      // this.Tcanvas.getActiveObject().setSelectionStyles({textAlign: 'Right'}).setCoords();
      this.Tcanvas.getActiveObject().set({ textAlign: "right" }).setCoords();
      this.Tcanvas.renderAll();

  }

  alignJustify() {

      this.isAlignLeftButtom = false;
      this.isAlignCenterButtom = false;
      this.isAlignRightButtom = false;
      this.Tcanvas.getActiveObject().set({ textAlign: "Justify" }).setCoords();
           this.Tcanvas.renderAll();

  }
  copyButtom() {

      this.Tcanvas.getActiveObject().clone((cloned) => {
        this._clipboard = cloned;
      });

  }

  pasteButtom() {

      this._clipboard.clone((clonedObj) => {
        this.Tcanvas.discardActiveObject();
        clonedObj.set({
          left: clonedObj.left + 10,
          top: clonedObj.top + 10,
          evented: true,
        });
        if (clonedObj.type === "activeSelection") {
          // active selection needs a reference to the canvas.
          clonedObj.canvas = this.Tcanvas;
          clonedObj.forEachObject((obj) => {
            this.Tcanvas.add(obj);
          });
          // this should solve the unselectability
          clonedObj.setCoords();
        } else {
          this.Tcanvas.add(clonedObj);
        }
        this._clipboard.top += 10;
        this._clipboard.left += 10;
        this.Tcanvas.setActiveObject(clonedObj);

        this.Tcanvas.requestRenderAll();
      });

  }

  cutButtom() {
    if( this.Tcanvas.getActiveObject() === null){
      return;
    }
    this.Tcanvas.getActiveObject().clone((cloned)=> {
      this._clipboard = cloned;
      //remove after cloned to clipboard
      this.Tcanvas.remove( this.Tcanvas.getActiveObject());
    });


  }

  fontfamilyfunc(event: any) {
    this.Tcanvas.getActiveObject()
      .setSelectionStyles({ fontFamily: event })
      .setCoords();
    this.Tcanvas.renderAll();
  }

  fontSizefunc(event: any) {
    this.Tcanvas.getActiveObject()
      .setSelectionStyles({ fontSize: event })
      .setCoords();
    this.Tcanvas.renderAll();

  }
  StrokeLinefunc(event:any){

    let aObject= this.Tcanvas.getActiveObject();
    console.log(aObject);
    this.Tcanvas.freeDrawingBrush.width = event;

      if( aObject=== null || aObject === undefined )
      {
      return;
    }else{
      aObject.set('strokeWidth',parseInt(event));

    }
  }


  fowardButtom() {

      this.isFrontButtom = false;
      this.isBackwardsButtom = false;
      this.isBackButtom = false;

      this.activeObject = this.Tcanvas.getActiveObject();
      this.activeObject.bringForward();
           this.Tcanvas.renderAll();

  }
  frontButtom() {

      this.isFowardButtom = false;
      this.isBackwardsButtom = false;
      this.isBackButtom = false;

      this.activeObject = this.Tcanvas.getActiveObject();
      this.activeObject.bringToFront();
        this.Tcanvas.renderAll();

  }
  backwardsButtom() {

      this.isFowardButtom = false;
      this.isFrontButtom = false;
      this.isBackButtom = false;

      this.activeObject = this.Tcanvas.getActiveObject();
      this.activeObject.sendBackwards();

      this.Tcanvas.renderAll();

  }

  backButtom() {

      this.isFowardButtom = false;
      this.isFrontButtom = false;
      this.isBackwardsButtom = false;
      this.activeObject = this.Tcanvas.getActiveObject();
      this.activeObject.sendToBack();
      this.Tcanvas.renderAll();

  }
   strokeColor(){
    this.isStrokeColor=!this.isStrokeColor;
    console.log(this.isStrokeColor);

  }
  eventColor(event: any) {
    console.log(event);
    if (this.Tcanvas.getActiveObject() === undefined && !this.isStrokeColor) {
      this.Tcanvas.freeDrawingBrush.color = event;
    } else if (this.Tcanvas.getActiveObject()["text"] !== undefined && !this.isStrokeColor && this.Tcanvas.getActiveObject()["text"] !== null ) {
      this.Tcanvas.getActiveObject()
        .setSelectionStyles({ fill: event })
        .setCoords();
      this.Tcanvas.renderAll();
    }else if(this.Tcanvas.getActiveObject() !== undefined && this.isStrokeColor===true){
      this.Tcanvas.getActiveObject().set('stroke', event).setCoords();
    this.Tcanvas.renderAll();

    }

    else {
      this.Tcanvas.getActiveObject().set("fill", event).setCoords();
      this.Tcanvas.renderAll();
    }

    /*  this.Pctx.globalCompositeOperation = 'source-over';
    this.Pctx.strokeStyle = this.color;
    console.log(this.Pctx)*/

  }

  trashButton() {
   // this.isTrashButtom=!this.isTrashButtom;
    let activeObjects = this.Tcanvas.getActiveObjects();
    this.Tcanvas.discardActiveObject();
    if (activeObjects.length) {
      this.Tcanvas.remove.apply(this.Tcanvas, activeObjects);
    }

  }

  undoButtom() {

      this.Tcanvas.undo();


  }
  redoButtom() {

      this.Tcanvas.redo();


  }
  groupsButtom() {

      if (!this.Tcanvas.getActiveObject()) {
        return;
      }
      if (this.Tcanvas.getActiveObject().type !== "activeSelection") {
        return;
      }
      this.Tcanvas.getActiveObject().toGroup();
      this.Tcanvas.requestRenderAll();


  }
  ungroupsButtom() {

      if (!this.Tcanvas.getActiveObject()) {
        return;
      }
      if (this.Tcanvas.getActiveObject().type !== "group") {
        return;
      }
      this.Tcanvas.getActiveObject().toActiveSelection();
      this.Tcanvas.requestRenderAll();


  }
  selectAllButtom() {

      let canvas = this.Tcanvas;
      this.Tcanvas.discardActiveObject();
      let sel = new fabric.ActiveSelection(this.Tcanvas.getObjects(), {
        canvas,
      });
      this.Tcanvas.setActiveObject(sel);
      this.Tcanvas.requestRenderAll();


  }
  unSelectedButtom() {

      this.Tcanvas.discardActiveObject();
      this.Tcanvas.requestRenderAll();


  }

  gradientButtom() {
    this.isGradientButtom = !this.isGradientButtom;
  }
  lockButtom() {
    this.isLockButtom = !this.isLockButtom;
    let Aselection = this.Tcanvas.getActiveObject();
    console.log();
    if (this.isLockButtom) {
      Aselection.lockMovementX = true;
      Aselection.lockMovementY = true;
      Aselection.lockRotation = true;
      Aselection.lockScalingFlip = true;
      Aselection.lockScalingX = true;
      Aselection.lockScalingY = true;
      Aselection.lockSkewingX = true;
      Aselection.lockSkewingY = true;
      Aselection.hasBorders = true;
      Aselection.hasControls = true;
      Aselection.selectable = true;

    } else {
      Aselection.lockMovementX = false;
      Aselection.lockMovementY = false;
      Aselection.lockRotation = false;
      Aselection.lockScalingFlip = false;
      Aselection.lockScalingX = false;
      Aselection.lockScalingY = false;
      Aselection.lockSkewingX = false;
      Aselection.lockSkewingY = false;
      Aselection.hasBorders = true;
      Aselection.hasControls = true;

    }
  }

fEvents(){this.Tcanvas.on('object:removed',  ()=> {
  this.socket.emit('remove', JSON.stringify(this.Tcanvas));
});}
}
