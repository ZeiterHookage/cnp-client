import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackboardToolbarComponent } from './blackboard-toolbar.component';

describe('BlackboardToolbarComponent', () => {
  let component: BlackboardToolbarComponent;
  let fixture: ComponentFixture<BlackboardToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlackboardToolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackboardToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
