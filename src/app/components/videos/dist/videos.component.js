"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.VideosComponent = void 0;
var core_1 = require("@angular/core");
var environment_1 = require("src/environments/environment");
var rtcmulticonnection_1 = require("rtcmulticonnection");
var rxjs_1 = require("rxjs");
var video_1 = require("./video");
var VideosComponent = /** @class */ (function () {
    function VideosComponent(_renderer, mainService, sanitizer, plattformId) {
        this._renderer = _renderer;
        this.mainService = mainService;
        this.sanitizer = sanitizer;
        this.plattformId = plattformId;
        this.roomIdP = {};
        this.allRecordedBlobs = [];
        this.broadcastIdS = "";
    }
    VideosComponent.prototype.ngOnInit = function () {
        this.subs$ = new rxjs_1.Subscription();
        this.init();
    };
    VideosComponent.prototype.ngAfterViewInit = function () {
    };
    VideosComponent.prototype.ngAfterContentInit = function () { };
    VideosComponent.prototype.ngAfterViewChecked = function () { };
    VideosComponent.prototype.ngDoCheck = function () { };
    VideosComponent.prototype.init = function () {
        this.enableRecordings = false;
        this.url = environment_1.environment.wsUrl + "/";
        this.connection = new rtcmulticonnection_1["default"]();
        this.connection.socketURL = this.url;
        // https://www.rtcmulticonnection.org/docs/iceServers/
        // use your own TURN-server here!
        // its mandatory in v3
        this.connection.enableScalableBroadcast = true;
        // each relaying-user should serve only 1 users
        this.connection.maxRelayLimitPerUser = 1;
        // we don't need to keep room-opened
        // scalable-broadcast.js will handle stuff itself.
        this.connection.autoCloseEntireSession = true;
        this.connection.socketMessageEvent = "scalable-media-broadcast-demo";
        this.download(this.connection);
    };
    VideosComponent.prototype.download = function (connection) {
        var id = this.mainService.getTaRR();
        var broadcastId = id["room"];
        var connectionAndBrocastId = video_1.videoFunction(connection, broadcastId);
        var connections = connectionAndBrocastId["connection"];
        var broadcastIds = connectionAndBrocastId["broadcastId"];
        this.mainService.handleEvent(connections, broadcastIds);
    };
    VideosComponent.prototype.ngOnDestroy = function () {
        this.subs$.unsubscribe();
        this.connection = {};
    };
    __decorate([
        core_1.Input()
    ], VideosComponent.prototype, "sender");
    VideosComponent = __decorate([
        core_1.Component({
            selector: "app-videos",
            templateUrl: "./videos.component.html",
            styleUrls: ["./videos.component.css"]
        }),
        __param(3, core_1.Inject(core_1.PLATFORM_ID))
    ], VideosComponent);
    return VideosComponent;
}());
exports.VideosComponent = VideosComponent;
