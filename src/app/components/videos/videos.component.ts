import {
  Component,
  Input,
  OnInit,
  Renderer2,
  PLATFORM_ID,
  Inject,
  AfterViewInit,
  OnDestroy,
  AfterContentInit,
  AfterViewChecked,
  DoCheck,
} from "@angular/core";
import { MainServicesService } from "../../services/main-services.service";
import {
  DomSanitizer,
  SafeUrl,
  SafeHtml,
  SafeScript,
} from "@angular/platform-browser";
import { environment } from "src/environments/environment";
import RTCMulticonnection from "rtcmulticonnection";
import { Subscription } from "rxjs";

import { videoFunction } from "./video";
@Component({
  selector: "app-videos",
  templateUrl: "./videos.component.html",
  styleUrls: ["./videos.component.css"],
})
export class VideosComponent
  implements
    OnInit,
    AfterViewInit,
    OnDestroy,
    AfterContentInit,
    AfterViewChecked,
    DoCheck {
  //@ViewChild("videoPreview") videoPreview: ElementRef<HTMLVideoElement>;
  @Input() sender: boolean;

  videoGrid: any;
  videoContent: SafeHtml;
  scriptContent: SafeScript;
  url: any;
  urlVideoContent: any;
  connection: any;
  roomIdP: any = {};
  open: any;
  enableRecordings: boolean;
  videoPreview: any;
  broadcastId: any;
  allRecordedBlobs: any = [];
  broadcastIdS = "";
  subs$: Subscription;
  constructor(
    private _renderer: Renderer2,
    private mainService: MainServicesService,
    private sanitizer: DomSanitizer,
    @Inject(PLATFORM_ID) private plattformId
  ) {}

  ngOnInit() {
    this.subs$ = new Subscription();
    this.init();

  }
  ngAfterViewInit() {

  }
  ngAfterContentInit() {}
  ngAfterViewChecked() {}
  ngDoCheck() {}


  init() {


      this.enableRecordings = false;
      this.url = environment.wsUrl + "/";
      this.connection = new RTCMulticonnection();

      this.connection.socketURL = this.url;
      // https://www.rtcmulticonnection.org/docs/iceServers/
      // use your own TURN-server here!

      // its mandatory in v3
      this.connection.enableScalableBroadcast = true;

      // each relaying-user should serve only 1 users
      this.connection.maxRelayLimitPerUser = 1;

      // we don't need to keep room-opened
      // scalable-broadcast.js will handle stuff itself.
      this.connection.autoCloseEntireSession = true;

      this.connection.socketMessageEvent = "scalable-media-broadcast-demo";
      this.download(this.connection);

  }
   download(connection: RTCMulticonnection) {
    let id = this.mainService.getTaRR();


        let broadcastId = id["room"];
        let connectionAndBrocastId = videoFunction(connection, broadcastId);
        let connections = connectionAndBrocastId["connection"];
        let broadcastIds = connectionAndBrocastId["broadcastId"];
        this.mainService.handleEvent(connections, broadcastIds);


  }
  ngOnDestroy() {
    this.subs$.unsubscribe();
    this.connection = {};
  }
}
