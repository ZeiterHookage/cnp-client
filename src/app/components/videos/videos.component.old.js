import { Component, Input, OnInit, AfterViewInit } from '@angular/core';

@Component({
    selector: 'app-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit, AfterViewInit {
    @Input() sender: boolean;



    video1: any;
    video2: any;

    iceServers: any;
    streamConstrains: any;
    localStream: any;
    pc1Local: RTCPeerConnection;
    pc1Remote: RTCPeerConnection;
    pc2Local: RTCPeerConnection;
    pc2Remote: RTCPeerConnection;
    offerOptions: any = {
        offerToReceiveAudio: 1,
        offerToReceiveVideo: 1
    };
    constructor() {
        navigator["getUserMedia"] =
            navigator["getUserMedia"] ||
            navigator["webkitGetUserMedia"] ||
            navigator["webkitGetUserMedia"];
        this.streamConstrains = {
            audio: true,
            video: true,
        };


    }

    ngOnInit(): void {
        this.video1 = document.querySelector('#transmiter');
        this.video2 = document.querySelector('#reciever');

        this.iceServers = {
            iceServer: [
                { urls: "stun:stun.services.mozilla.com" },
                { urls: "stun:stun.l.google.com:19302" },
            ],
        };

        this.start();
    }

    ngAfterViewInit() {}


    start() {

        navigator.mediaDevices.getUserMedia(this.streamConstrains)
            .then(stream => this.gotStream(stream))
            .catch(e => console.log('getUserMedia() error: ', e));
    }

    gotStream(stream) {
        console.log('Received local stream');
        this.video1.srcObject = stream;
        this.localStream = stream;
        // this.callButton.disabled = false;

    }



    call() {

        console.log('Starting calls');
        const audioTracks = this.localStream.getAudioTracks();
        const videoTracks = this.localStream.getVideoTracks();
        if (audioTracks.length > 0) {
            console.log(`Using audio device: ${audioTracks[0].label}`);
        }
        if (videoTracks.length > 0) {
            console.log(`Using video device: ${videoTracks[0].label}`);
        }
        // Create an RTCPeerConnection via the polyfill.
        const servers = null;
        this.pc1Local = new RTCPeerConnection(this.iceServers);
        this.pc1Remote = new RTCPeerConnection(this.iceServers);



        this.pc1Remote.ontrack = this.gotRemoteStream1;


        console.log('pc1: created local and remote peer connection objects');


        this.localStream.getTracks().forEach(track => this.pc1Local.addTrack(track, this.localStream));
        console.log('Adding local stream to pc1Local');
        this.pc1Local.createOffer()
            .then((sessionDescription) => {

                this.pc1Local.setLocalDescription(sessionDescription)
            })
            .catch(err => this.onCreateSessionDescriptionError(err));

        this.pc1Local.onicecandidate = this.iceCallback1Local;
        this.pc1Remote.onicecandidate = this.iceCallback1Remote;
    }

    onCreateSessionDescriptionError(error) {
        console.log(`Failed to create session description: ${error.toString()}`);
    }

    gotDescription1Local(desc) {
        console.log('desc', desc);
        this.pc1Local.setLocalDescription(desc);
        console.log(`Offer from pc1Local\n${desc.sdp}`);
        this.pc1Local.setRemoteDescription(desc);
        this.pc1Remote.setRemoteDescription(
            new RTCSessionDescription(desc.sdp)
        )




        this.pc1Remote.createAnswer().then((sessionDescription) => {
            console.log("sending answer", sessionDescription);
            this.pc1Remote.setLocalDescription(sessionDescription);

        }).catch(err => this.onCreateSessionDescriptionError(err));
    }


    hangup() {
        console.log('Ending calls');
        //this.pc1Local.close();
        this.pc1Remote.close();
        this.pc2Local.close();
        this.pc2Remote.close();
        this.pc1Local = this.pc1Remote = null;
        this.pc2Local = this.pc2Remote = null;

    }

    gotRemoteStream1(e) {

        if (this.video2.srcObject !== e.streams[0]) {
            this.video2.srcObject = e.streams[0];
            console.log('pc2: received remote stream');
        }

    }
    iceCallback1Local(event) {
        console.log(event);
        this.handleCandidate(event.candidate, this.pc1Remote, 'pc1: ', 'local');
    }

    iceCallback1Remote(event) {
        this.handleCandidate(event.candidate, this.pc1Local, 'pc1: ', 'remote');
    }

    handleCandidate(candidate, dest, prefix, type) {
        dest.addIceCandidate(candidate['candidate'])
            .then(() => this.onAddIceCandidateSuccess())
            .catch((err) => this.onAddIceCandidateError(err));
        console.log(`${prefix}New ${type} ICE candidate: ${candidate ? candidate.candidate : '(null)'}`);
    }

    onAddIceCandidateSuccess() {
        console.log('AddIceCandidate success.');
    }

    onAddIceCandidateError(error) {
        console.log(`Failed to add ICE candidate: ${error.toString()}`);
    }

}