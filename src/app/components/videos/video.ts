import RTCMulticonnection from "rtcmulticonnection";


export function videoFunction(
  connection: RTCMulticonnection,broadcastId:any):RTCMulticonnection
 {


   connection.connectSocket(function(socket) {
    socket.on("logs", function (log) {
      console.log(log);
    });

    // this event is emitted when a broadcast is already created.
    socket.on("join-broadcaster", function (hintsToJoinBroadcast) {
      console.log("join-broadcaster", hintsToJoinBroadcast);

      connection.session = hintsToJoinBroadcast.typeOfStreams;
      connection.sdpConstraints.mandatory = {
        OfferToReceiveVideo: !!connection.session.video,
        OfferToReceiveAudio: !!connection.session.audio,
      };
      connection.broadcastId = hintsToJoinBroadcast.broadcastId;
      connection.join(hintsToJoinBroadcast.userid);
    });

    socket.on("rejoin-broadcast", function (broadcastId) {
      console.log("rejoin-broadcast", broadcastId);

      connection.attachStreams = [];
      // tslint:disable-next-line: typedef
      socket.emit("check-broadcast-presence", broadcastId, function (
        isBroadcastExists
      ) {
        if (!isBroadcastExists) {
          // the first person (i.e. real-broadcaster) MUST set his user-id
          connection.userid = broadcastId;
        }

        socket.emit("join-broadcast", {
          broadcastId: broadcastId,
          userid: connection.userid,
          typeOfStreams: connection.session,
        });
      });
    });

    socket.on("broadcast-stopped", function (broadcastId) {
      // alert('Broadcast has been stopped.');
      // location.reload();
      console.error("broadcast-stopped", broadcastId);
      alert("This broadcast has been stopped.");
    });

    // this event is emitted when a broadcast is absent.
    socket.on("start-broadcasting", function (typeOfStreams) {
      console.log("start-broadcasting", typeOfStreams);

      // host i.e. sender should always use this!
      connection.sdpConstraints.mandatory = {
        OfferToReceiveVideo: false,
        OfferToReceiveAudio: false,
      };
      connection.session = typeOfStreams;

      // "open" method here will capture media-stream
      // we can skip this function always; it is totally optional here.
      // we can use "connection.getUserMediaHandler" instead
      connection.open(connection.userid);
    });

  
  });
 
  
  console.log(broadcastId,connection);
  

 return {connection,broadcastId};
  
}



