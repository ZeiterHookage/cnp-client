import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbtoolbarComponent } from './bbtoolbar.component';

describe('BbtoolbarComponent', () => {
  let component: BbtoolbarComponent;
  let fixture: ComponentFixture<BbtoolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BbtoolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BbtoolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
