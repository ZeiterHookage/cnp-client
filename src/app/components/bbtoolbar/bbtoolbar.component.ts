import { from, fromEvent, fromEventPattern, Observable, Subject } from "rxjs";
import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from "@angular/core";
import { fabric } from "fabric";
import { filter, last } from "rxjs/operators";
import { Socket } from "ngx-socket-io";
import { v4 as uuidv4 } from "uuid";

@Component({
  selector: "app-bbtoolbar",
  templateUrl: "./bbtoolbar.component.html",
  styleUrls: ["./bbtoolbar.component.css"],
})
export class BbtoolbarComponent implements OnInit, OnDestroy {
  @Input() BBTcanvas: any;
  @Input() BBTctx: any;
  isPenButton = false;
  isSkewButton = false;
  isLoadedFromJson = false;
  myObs$: Subject<any>;
  canvas: any;
  is_creating: boolean;
  mouse_dragging: boolean;
  shape: any;
  mouseCoords;
  points: any;
  canvasScale = 1;
  SCALE_FACTOR = 1.01;
  // new pinter
  min: number;
  max: number;
  polygonMode: boolean;
  pointArray: any = [];
  lineArray: any = [];
  activeLine: any;
  activeShape: any;
  line: any;
  id: number;
  dataS = [];
  constructor(private socket: Socket) {}

  ngOnInit(): void {
    // this.fDrawButton();
  }

  pointerButton() {
    this.BBTcanvas.defaultCursor = "default";
    this.BBTcanvas.isDrawingMode = false;
  }
  icursorButton() {
    // this.BBTcanvas.style.cursor = "text";
    let text = new fabric.IText("aula-V", {
      width: 300,
      left: 100,
      top: 20,
    });
    text["id"] = uuidv4();
    this.BBTcanvas.add(text).setActiveObject(text);
  }

  drawButton() {
    this.BBTcanvas.isDrawingMode = true;
  }

  fDrawButton() {
    this.socket.on("DrawingMode", (isdrawing) => {
      console.log(isdrawing);
      this.BBTcanvas.isDrawingMode = isdrawing;
    });
  }

  squareButton() {
    let rect = new fabric.Rect({
      left: 40,
      top: 40,
      width: 50,
      height: 50,
      fill: "transparent",
      stroke: "black",
      strokeWidth: 1,
    });
    rect["id"] = uuidv4();
    this.BBTcanvas.add(rect);
  }

  starButton() {
    this.points = this.starPolygonPoints(5, 50, 25);
    let myStar = new fabric.Polygon(this.points, {
      stroke: "black",
      left: 100,
      top: 10,
      strokeWidth: 1,
      fill: "transparent",
      //angle: Math.PI / 2 * 3
      // strokeLineJoin: 'bevil'
    });
    myStar["id"] = uuidv4();
    this.BBTcanvas.add(myStar);
  }

  polyButton() {
    this.points = this.regularPolygonPoints(6, 30);
    let myPoly = new fabric.Polygon(this.points, {
      stroke: "black",
      left: 10,
      top: 10,
      strokeWidth: 1,
      fill: "transparent",
      hasControls: true,
      //strokeLineJoin: 'bevil'
    });
    myPoly["id"] = uuidv4();
    this.BBTcanvas.add(myPoly);
  }

  ///////////penTool

  penButton() {
    this.drawPolygon();
    this.isPenButton = !this.isPenButton;
    if (this.isPenButton) {
      this.BBTcanvas.on("mouse:down", (options) => {
        if (this.polygonMode) {
          this.addPoint(options);
        }
        if (options.target && options.target.id === this.pointArray[0]["id"]) {
          console.log(this.polygonMode, this.pointArray);

          this.generatePolygon(this.pointArray);

          console.log(this.isPenButton, this.polygonMode, this.pointArray);
        }
      });

      this.BBTcanvas.on("mouse:move", (options) => {
        if (this.activeLine && this.activeLine.class === "line") {
          let pointer = this.BBTcanvas.getPointer(options.e);
          this.activeLine.set({ x2: pointer.x, y2: pointer.y });

          let points = this.activeShape.get("points");
          points[this.pointArray.length] = {
            x: pointer.x,
            y: pointer.y,
          };
          this.activeShape.set({
            points,
          });
          this.BBTcanvas.renderAll();
        }
        this.BBTcanvas.renderAll();
      });
    }
  }
  addPoint(options) {
    let id = uuidv4();
    let circle = new fabric.Circle({
      radius: 5,
      fill: "#ffffff",
      stroke: "#333333",
      strokeWidth: 0.5,
      left: options["pointer"]["x"],
      top: options["pointer"]["y"],
      selectable: false,
      hasBorders: false,
      hasControls: false,
      originX: "center",
      originY: "center",
      objectCaching: false,
    });
    circle["id"] = `${id}`;

    if (this.pointArray.length === 0) {
      circle.set({
        fill: "red",
      });
    }
    let points = [
      options["pointer"]["x"],
      options["pointer"]["y"],
      options["pointer"]["x"],
      options["pointer"]["y"],
    ];
    this.line = new fabric.Line(points, {
      strokeWidth: 2,
      fill: "#999999",
      stroke: "#999999",
      originX: "center",
      originY: "center",
      selectable: false,
      hasBorders: false,
      hasControls: false,
      evented: false,
      objectCaching: false,
    });
    this.line.set({ class: "line" });
    if (this.activeShape) {
      let pos = this.BBTcanvas.getPointer(options.e);
      let points = this.activeShape.get("points");
      points.push({
        x: pos.x,
        y: pos.y,
      });
      let polygon = new fabric.Polygon(points, {
        stroke: "#333333",
        strokeWidth: 1,
        fill: "transparent",
        opacity: 0.3,
        selectable: false,
        hasBorders: false,
        hasControls: false,
        evented: false,
        objectCaching: false,
      });
      this.BBTcanvas.remove(this.activeShape);
      this.BBTcanvas.add(polygon);
      this.activeShape = polygon;
      this.BBTcanvas.renderAll();
    } else {
      let polyPoint = [
        {
          x: options["pointer"]["x"],
          y: options["pointer"]["y"],
        },
      ];
      let polygon = new fabric.Polygon(polyPoint, {
        stroke: "#333333",
        strokeWidth: 1,
        fill: "transparent",
        opacity: 0.3,
        selectable: false,
        hasBorders: false,
        hasControls: false,
        evented: false,
        objectCaching: false,
      });
      this.activeShape = polygon;
      this.BBTcanvas.add(polygon);
    }
    this.activeLine = this.line;

    this.pointArray.push(circle);
    this.lineArray.push(this.line);

    this.BBTcanvas.add(this.line);
    this.BBTcanvas.add(circle);
    this.BBTcanvas.selection = true;
  }

  generatePolygon(pointArray) {
    let points: any = [];

    pointArray.forEach((point) => {
      points.push({ x: point["left"], y: point["top"] });

      this.BBTcanvas.remove(point);
    });

    this.lineArray.forEach((line, index) => {
      console.log(line, index);
      this.BBTcanvas.remove(line);
    });

    this.BBTcanvas.remove(this.activeShape).remove(this.activeLine);
    let polygon = new fabric.Polygon(points, {
      stroke: "black",
      strokeWidth: 1,
      fill: "transparent",
      opacity: 1,
      hasBorders: true,
      hasControls: true,
      selectable: true,
      objectCaching: true,
    });
    polygon["id"] = uuidv4();
    this.BBTcanvas.add(polygon);
    this.isPenButton = null;
    this.polygonMode = false;
    this.BBTcanvas.selection = true;
    /*let polyn = this.BBTcanvas.getActiveObject();
    console.log(polyn);
    let polynCenter = polyn.getCenterPoint();
    let translatedPoints = polyn.get("points").map(function (p) {
      return {
        x: polynCenter.x + p.x,
        y: polynCenter.y + p.y,
      };
    });

    translatedPoints.forEach(function (p) {
      this.BBTctx.getContext().strokeRect(p.x - 5, p.y - 5, 10, 10);
    });*/

    //this.drawPolygon();
    //this.isPenButton= true;
  }

  drawPolygon() {
    this.pointArray = [];
    this.lineArray = [];
    this.BBTcanvas.off("mouse:down");
    this.activeShape = null;
    this.activeLine = null;
    this.polygonMode = true;
  }

  skewButton() {
    this.isSkewButton = !this.isSkewButton;

    if (this.isSkewButton) {
      let aObject = this.BBTcanvas.getActiveObject();

      if (aObject === null || aObject === undefined) {
        return;
      } else {
        this.BBTcanvas.on("mouse:move", (options) => {


        aObject.set("skewX", options["pointer"]["x"]).setCoords();
        this.BBTcanvas.requestRenderAll();
 });
        this.BBTcanvas.on("mouse:up", () => {
          this.isSkewButton = false;
          this.BBTcanvas.off("mouse:move");
        });
      }

      this.BBTcanvas.on("object:skewed", (options) => {
        console.log(options);

      });
      //const lastPoint = { x: null, y: null };

      /*: options.e.clientX < lastPoint.x ? 'left'
          : 'none'*/

      /*const upOrDown =
          options.e.clientY > lastPoint.y
            ? "down"
            : options.e.clientY < lastPoint.y
            ? "up"
            : "none";*/

      /*
          here you can apply the transformations you need to,
          then update the cursor position tracker for the
          next iteration
        */

      // lastPoint.y = options.e.clientY;
    } else {
    }
  }
  //circleButtom

  circleButton() {
    let circle = new fabric.Circle({
      left: 40,
      top: 40,
      radius: 50,
      fill: "transparent",
      stroke: "black",
      strokeWidth: 1,
      dirty: true,
    });
    circle["id"] = uuidv4();
    console.log("circle", circle);

    this.stringifyCanvas();
    this.BBTcanvas.add(circle);
  }

  triangleButton() {
    let triangle = new fabric.Triangle({
      fill: "transparent",
      stroke: "black",
      strokeWidth: 1,
    });
    triangle["id"] = uuidv4();
    this.BBTcanvas.add(triangle);
  }

  plusButton() {
    this.zoomOut();
  }

  minusButton() {
    this.zoomIn();
  }
  // Zoom
  zoomIn() {
    this.canvasScale = this.canvasScale * this.SCALE_FACTOR;

    this.BBTcanvas.setHeight(this.BBTcanvas.getHeight() * this.SCALE_FACTOR);
    this.BBTcanvas.setWidth(this.BBTcanvas.getWidth() * this.SCALE_FACTOR);

    let objects = this.BBTcanvas.getObjects();
    for (let i in objects) {
      let scaleX = objects[i].scaleX;
      let scaleY = objects[i].scaleY;
      let left = objects[i].left;
      let top = objects[i].top;

      let tempScaleX = scaleX * this.SCALE_FACTOR;
      let tempScaleY = scaleY * this.SCALE_FACTOR;
      let tempLeft = left * this.SCALE_FACTOR;
      let tempTop = top * this.SCALE_FACTOR;

      objects[i].scaleX = tempScaleX;
      objects[i].scaleY = tempScaleY;
      objects[i].left = tempLeft;
      objects[i].top = tempTop;

      objects[i].setCoords();
    }

    this.BBTcanvas.renderAll();
  }

  zoomOut() {
    this.canvasScale = this.canvasScale / this.SCALE_FACTOR;

    this.BBTcanvas.setHeight(
      this.BBTcanvas.getHeight() * (1 / this.SCALE_FACTOR)
    );
    this.BBTcanvas.setWidth(
      this.BBTcanvas.getWidth() * (1 / this.SCALE_FACTOR)
    );

    let objects = this.BBTcanvas.getObjects();
    for (let i in objects) {
      let scaleX = objects[i].scaleX;
      let scaleY = objects[i].scaleY;
      let left = objects[i].left;
      let top = objects[i].top;

      let tempScaleX = scaleX * (1 / this.SCALE_FACTOR);
      let tempScaleY = scaleY * (1 / this.SCALE_FACTOR);
      let tempLeft = left * (1 / this.SCALE_FACTOR);
      let tempTop = top * (1 / this.SCALE_FACTOR);

      objects[i].scaleX = tempScaleX;
      objects[i].scaleY = tempScaleY;
      objects[i].left = tempLeft;
      objects[i].top = tempTop;

      objects[i].setCoords();
    }

    this.BBTcanvas.renderAll();
  }

  // start and polygon's buttom functions
  regularPolygonPoints(sideCount, radius) {
    let sweep = (Math.PI * 2) / sideCount;
    let cx = radius;
    let cy = radius;
    let points = [];
    for (let i = 0; i < sideCount; i++) {
      let x = cx + radius * Math.cos(i * sweep);
      let y = cy + radius * Math.sin(i * sweep);
      points.push({ x, y });
    }
    return points;
  }

  starPolygonPoints(spikeCount, outerRadius, innerRadius) {
    let rot = (Math.PI / 2) * 3;
    let cx = outerRadius;
    let cy = outerRadius;
    let sweep = Math.PI / spikeCount;
    let points = [];
    let angle = 0.95;

    for (let i = 0; i < spikeCount; i++) {
      let x = cx + Math.cos(angle) * outerRadius;
      let y = cy + Math.sin(angle) * outerRadius;
      points.push({ x, y });
      angle += +sweep;

      x = cx + Math.cos(angle) * innerRadius;
      y = cy + Math.sin(angle) * innerRadius;
      points.push({ x, y });
      angle += sweep;
    }
    return points;
  }

  fEvents() {
    this.BBTcanvas.on("object:removed", () => {
      this.socket.emit("remove", this.stringifyCanvas());
    });

    this.socket.fromEvent("remove").subscribe((data) => {
      this.renderCanvas(data);
    });
  }

  stringifyCanvas() {
    return this.BBTcanvas.toJSON();
  }
  renderCanvas(data) {
    console.log(data);
    this.BBTcanvas.loadFromJSON(
      data,
      this.BBTcanvas.renderAll.bind(this.BBTcanvas)
    );
  }
  ngOnDestroy() {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
     this.drawPolygon();
  }
}
