import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MainServicesService } from "../../services/main-services.service";
import { Login } from "../register/register.inteface";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  user: Login = {
    email: "",
    password: "",
    qrcode:''
  };

  image:any;
  id:any;
  constructor(
    public mainService: MainServicesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.image= new Image();
  this.Canvas();
  }
Canvas(){

    const PostCanva= this.mainService.postCanvas();
    const canvas =  document.getElementById('myCanvas') as HTMLCanvasElement;

    const ctx = canvas.getContext('2d');

    PostCanva.subscribe(secrets=> {
      this.id= secrets['id'];
      console.log(secrets);

    ctx.font = "12px Arial";
    /*ctx['width'] = naturalWidth; // or 'width' if you want a special/scaled size
    ctx['height'] = naturalHeight;*/

    this.image.src = secrets['image_data'];
      this.image.addEventListener('load', ()=> {
    ctx.drawImage(this.image, 0, 0, canvas['width'], canvas['height']);  // resolve(ctx.getImageData(0, 0, canvas.width, canvas.height));
    /*ctx.fillText(secrets['tempSecret'], 23, 20);*/
    ctx.textAlign = 'center';
      }, false);
    });

    return ctx;




  }
  login(forma: NgForm) {
    if (forma.invalid) {
      Object.values(forma.controls).forEach((control) => {
        control.markAllAsTouched();
      });
      return;
    } else {
      let login = forma.value;
      if(login !== undefined || login !== null)
      {
      let email = login['email'].replace(/(^"|"$)/g, "");
      let password = login['password'].replace(/(^"|"$)/g, "");
      let sms = login['qrcode'].replace(/(^"|"$)/g, "");
    /*  if( (this.id !== null || this.id !== undefined) && (token !== null || token !== undefined))
    {

            console.log(this.id)
            const secrectCanvas=this.mainService.secretCanvas(this.id, token);
            secrectCanvas.subscribe(console.log);
    }*/
      this.mainService.postLogin(email, password,sms,this.id).pipe(
        map((x) => {
          // Map operator tranforming the data into a object
          console.log('login',x);
          const logIn = {};
          Object.defineProperties(logIn, {
            token: {
              value: x["token"],
              writable: true,
            },
            role: {
              value: x["user"]["role"],
              writable: true,
            },
            ok: {
              value: x["ok"],
              writable: true,
            },
            date: {
              value: new Date().getTime(),
              writable: true,
            },
            name: {
              value: x["user"]["firstname"] + " " + x["user"]["lastname"],
              writable: true,
            },
            id: {
              value: x["user"]["_id"],
              writable: true,
            },
            img:{
              value: x['user']['img'],
              writable:true
            }
          });

          if (
            logIn["role"] === "USER_ROLE" ||
            logIn["role"] === "TEACHER_ROLE"
          ) {
            Object.defineProperty(logIn, "room", {
              value: x["user"]["grade"] + "-" + x["user"]["roomLetter"],
              writable: false,
              enumerable: true,
              configurable: false,
            });
          }
          return logIn;

        })).subscribe((user) =>this.mainService.getTokenRoleRoom(user));

}

    }


  }
  signOut() {
    /*let auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
        console.log('User signed out.');
    });*/
}

}
