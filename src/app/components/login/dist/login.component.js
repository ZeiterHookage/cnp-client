"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoginComponent = void 0;
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(mainService, router) {
        this.mainService = mainService;
        this.router = router;
        this.user = {
            email: "",
            password: "",
            qrcode: ''
        };
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.image = new Image();
        this.Canvas();
    };
    LoginComponent.prototype.Canvas = function () {
        var _this = this;
        var PostCanva = this.mainService.postCanvas();
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        PostCanva.subscribe(function (secrets) {
            _this.id = secrets['id'];
            console.log(secrets);
            ctx.font = "12px Arial";
            /*ctx['width'] = naturalWidth; // or 'width' if you want a special/scaled size
            ctx['height'] = naturalHeight;*/
            _this.image.src = secrets['image_data'];
            _this.image.addEventListener('load', function () {
                ctx.drawImage(_this.image, 0, 0, canvas['width'], canvas['height']); // resolve(ctx.getImageData(0, 0, canvas.width, canvas.height));
                /*ctx.fillText(secrets['tempSecret'], 23, 20);*/
                ctx.textAlign = 'center';
            }, false);
        });
        return ctx;
    };
    LoginComponent.prototype.login = function (forma) {
        var _this = this;
        if (forma.invalid) {
            Object.values(forma.controls).forEach(function (control) {
                control.markAllAsTouched();
            });
            return;
        }
        else {
            var login = forma.value;
            if (login !== undefined || login !== null) {
                var email = login['email'].replace(/(^"|"$)/g, "");
                var password = login['password'].replace(/(^"|"$)/g, "");
                var sms = login['qrcode'].replace(/(^"|"$)/g, "");
                /*  if( (this.id !== null || this.id !== undefined) && (token !== null || token !== undefined))
                {
            
                        console.log(this.id)
                        const secrectCanvas=this.mainService.secretCanvas(this.id, token);
                        secrectCanvas.subscribe(console.log);
                }*/
                this.mainService.postLogin(email, password, sms, this.id).pipe(operators_1.map(function (x) {
                    // Map operator tranforming the data into a object
                    console.log('login', x);
                    var logIn = {};
                    Object.defineProperties(logIn, {
                        token: {
                            value: x["token"],
                            writable: true
                        },
                        role: {
                            value: x["user"]["role"],
                            writable: true
                        },
                        ok: {
                            value: x["ok"],
                            writable: true
                        },
                        date: {
                            value: new Date().getTime(),
                            writable: true
                        },
                        name: {
                            value: x["user"]["firstname"] + " " + x["user"]["lastname"],
                            writable: true
                        },
                        id: {
                            value: x["user"]["_id"],
                            writable: true
                        },
                        img: {
                            value: x['user']['img'],
                            writable: true
                        }
                    });
                    if (logIn["role"] === "USER_ROLE" ||
                        logIn["role"] === "TEACHER_ROLE") {
                        Object.defineProperty(logIn, "room", {
                            value: x["user"]["grade"] + "-" + x["user"]["roomLetter"],
                            writable: false,
                            enumerable: true,
                            configurable: false
                        });
                    }
                    return logIn;
                })).subscribe(function (user) { return _this.mainService.getTokenRoleRoom(user); });
            }
        }
    };
    LoginComponent.prototype.signOut = function () {
        /*let auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function() {
            console.log('User signed out.');
        });*/
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: "app-login",
            templateUrl: "./login.component.html",
            styleUrls: ["./login.component.css"]
        })
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
