import { MainServicesService } from 'src/app/services/main-services.service';

import { VirtualScrollStrategy } from '@angular/cdk/scrolling';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Socket } from "ngx-socket-io";
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-virtual-scroll',
  templateUrl: './virtual-scroll.component.html',
  styleUrls: ['./virtual-scroll.component.css']
})
export class VirtualScrollComponent implements OnInit {
@Input() dataChat:any;
personas:any;
user:any={};
myObservableArray: Observable<any>;
data: Subject<any> = new Subject<any>();
subscription: Subscription =new Subscription();
  constructor(public socket: Socket,public mainService:MainServicesService) {
    this.personas = Array(500).fill(0);
    this.getDataChat();
  }

  ngOnInit(): void {

    console.log(this.mainService.getTaRR());
    this.peopleList();
 }
 getDataChat() {
  if (!this.myObservableArray) {

  this.myObservableArray = this.getData();
  this.subscription.add(this.myObservableArray.subscribe());

  }
}
getData(): Subject<any> {
  const interval = setInterval(() => {
  this.data.next(this.dataChat);
  this.subscription.add(this.data.subscribe());
  }, 500);
  () => clearInterval(interval);
  return this.data;
}

trackByFn(index: number, item: any): any {
  return index - 1;
}


 peopleList() {


    this.socket.on("enterToRoom", (resp) => {
      console.log("virtual scroll  Usuarios Conectados", resp);
    });


    this.socket.on("privateMessage", (message: any) => {
      console.log("private Message:", message);
    });

  // listen users's changes
  //when someone left the Room
  this.socket.on("peopleList", (people) => {
    console.log("virtual scroll peopleList", people);

  });

  // private Message

}
}
