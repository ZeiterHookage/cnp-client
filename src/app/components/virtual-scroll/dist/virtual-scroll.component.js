"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.VirtualScrollComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var VirtualScrollComponent = /** @class */ (function () {
    function VirtualScrollComponent(socket, mainService) {
        this.socket = socket;
        this.mainService = mainService;
        this.user = {};
        this.data = new rxjs_1.Subject();
        this.subscription = new rxjs_1.Subscription();
        this.personas = Array(500).fill(0);
        this.getDataChat();
    }
    VirtualScrollComponent.prototype.ngOnInit = function () {
        console.log(this.mainService.getTaRR());
        this.peopleList();
    };
    VirtualScrollComponent.prototype.getDataChat = function () {
        if (!this.myObservableArray) {
            this.myObservableArray = this.getData();
            this.subscription.add(this.myObservableArray.subscribe());
        }
    };
    VirtualScrollComponent.prototype.getData = function () {
        var _this = this;
        var interval = setInterval(function () {
            _this.data.next(_this.dataChat);
            _this.subscription.add(_this.data.subscribe());
        }, 500);
        (function () { return clearInterval(interval); });
        return this.data;
    };
    VirtualScrollComponent.prototype.trackByFn = function (index, item) {
        return index - 1;
    };
    VirtualScrollComponent.prototype.peopleList = function () {
        this.socket.on("enterToRoom", function (resp) {
            console.log("virtual scroll  Usuarios Conectados", resp);
        });
        this.socket.on("privateMessage", function (message) {
            console.log("private Message:", message);
        });
        // listen users's changes
        //when someone left the Room
        this.socket.on("peopleList", function (people) {
            console.log("virtual scroll peopleList", people);
        });
        // private Message
    };
    __decorate([
        core_1.Input()
    ], VirtualScrollComponent.prototype, "dataChat");
    VirtualScrollComponent = __decorate([
        core_1.Component({
            selector: 'app-virtual-scroll',
            templateUrl: './virtual-scroll.component.html',
            styleUrls: ['./virtual-scroll.component.css']
        })
    ], VirtualScrollComponent);
    return VirtualScrollComponent;
}());
exports.VirtualScrollComponent = VirtualScrollComponent;
