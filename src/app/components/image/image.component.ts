import { Component, OnInit, Input } from '@angular/core';
import {fabric} from 'fabric';
import { Socket } from "ngx-socket-io";
@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  @Input()Icanvas:any;
  @Input()Ictx:any;

  myImage: any;
  imgInput:any;
  constructor(private socket: Socket) { }

  ngOnInit(): void {
    this.imgInput= document.getElementById('imageInput');
  this.loadImg(this.Icanvas,this.Ictx);


  }

 loadImg(canvas: any, ctx: any) {
   this.imgInput.addEventListener("change", (e) => {
    let reader = new FileReader();
    reader.onload =  (event)=> {
      this.myImage = new Image();
      this.myImage.src = event.target.result;
      this.myImage.onload =  ()=> {
            let image = new fabric.Image( this.myImage);
            image.set({
                left: 100,
                top: 100,
                angle: 0,

            });
            canvas.add(image);

        }

    }
    reader.readAsDataURL(e.target.files[0]);

    });


  }

}
