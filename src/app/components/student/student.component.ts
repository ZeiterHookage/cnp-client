import { Socket } from 'ngx-socket-io';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {
  Component,
  Input,
  OnInit,
  OnDestroy, ViewChild
} from "@angular/core";
import { Observable, Subject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: "app-student",
  templateUrl: "./student.component.html",
  styleUrls: ["./student.component.css"],
})
export class StudentComponent implements OnInit, OnDestroy {
  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
  @Input() dataChat: any;
  myObservableArray: Observable<any>;
  data: Subject<any> = new Subject<any>();
 subscription: Subscription =new Subscription();
  constructor(private socket: Socket) {
    this.getDataChat();
  }

  ngOnInit() {

  }


  getDataChat() {
    if (!this.myObservableArray) {

    this.myObservableArray = this.getData();
    this.subscription.add(this.myObservableArray.subscribe());

    }
  }
  getData(): Subject<any> {
    const interval = setInterval(() => {
    this.data.next(this.dataChat);
    this.subscription.add(this.data.subscribe());
    }, 500);
    () => clearInterval(interval);
    return this.data;
  }

  trackByFn(index: number, item: any): any {
    return index - 1;
  }


  ngOnDestroy() {

   this.subscription.unsubscribe();

  }
}
