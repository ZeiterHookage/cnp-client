import { Component, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { delay} from "rxjs/operators";
import { MainServicesService } from "src/app/services/main-services.service";



@Component({
  selector: "app-teachers",
  templateUrl: "./teachers.component.html",
  styleUrls: ["./teachers.component.css"],
})
export class TeachersComponent implements OnInit, OnDestroy {
  roomData: any;
  user: object;
  chatListClass: boolean = true;
  isCamera: boolean = false;
  isList: boolean = false;
  isBlackboard: boolean = false;
  isChat: boolean = false;
  hotListenerMousedown: any;
  hotListenerMousemove: any;
  hotListenerMouseout: any;
  hotListenerMouseup: any;
  title: string;
  Obs$: Observable<any>;
  ObsSubc: Subscription = new Subscription();
  myObs$: Observable<any>;
  chatEvents: any = {};
  constructor(
    private mainService: MainServicesService,

  ) {}

  ngOnInit(): void {
    this.roomData = this.mainService.getTaRR();

    if (this.roomData !== undefined) {
      this.user = {
        name: this.roomData["name"],
        room: this.roomData["room"],
        id:this.roomData['id']
      };
    }
  }

  Camera() {
    this.isCamera = !this.isCamera;
    let obj={camera:this.isCamera , room: this.roomData['room']};
    this.myObs$ = new Observable((observer) => {
      observer.next(obj);
    });
    this.myObs$.pipe(delay(2000)).subscribe((obj) => {
      this.mainService.Camera(obj);
    });
  }

  list() {
    this.isList = !this.isList;
  }

  blackBoard() {
    this.isBlackboard = !this.isBlackboard;

    this.title = "Hello World";
    this.mainService.bBonTitle(this.title);
    let obj={bb:this.isBlackboard , room: this.roomData['room']};

    this.mainService.Blackboard(obj);
  }

  chat() {
    this.isChat = !this.isChat;
    let obj={chat: this.isChat , room: this.roomData['room']};

    this.mainService.Chat(obj);


  }
  listPeople(event:any){
    console.log('teacher',event);
  }

  chatEvent(e:Event){
    console.log('teacherEvent',e);
    this.Obs$ = new Observable((observer) => {
      observer.next(e);
    });
    this.ObsSubc = this.Obs$.subscribe((user) => this.chatEvents = user);
  }
  ngOnDestroy() {
    this.ObsSubc.unsubscribe();
  }

}
