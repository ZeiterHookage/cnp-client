"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TeachersComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var TeachersComponent = /** @class */ (function () {
    function TeachersComponent(mainService) {
        this.mainService = mainService;
        this.chatListClass = true;
        this.isCamera = false;
        this.isList = false;
        this.isBlackboard = false;
        this.isChat = false;
        this.ObsSubc = new rxjs_1.Subscription();
        this.chatEvents = {};
    }
    TeachersComponent.prototype.ngOnInit = function () {
        this.roomData = this.mainService.getTaRR();
        if (this.roomData !== undefined) {
            this.user = {
                name: this.roomData["name"],
                room: this.roomData["room"],
                id: this.roomData['id']
            };
        }
    };
    TeachersComponent.prototype.Camera = function () {
        var _this = this;
        this.isCamera = !this.isCamera;
        var obj = { camera: this.isCamera, room: this.roomData['room'] };
        this.myObs$ = new rxjs_1.Observable(function (observer) {
            observer.next(obj);
        });
        this.myObs$.pipe(operators_1.delay(2000)).subscribe(function (obj) {
            _this.mainService.Camera(obj);
        });
    };
    TeachersComponent.prototype.list = function () {
        this.isList = !this.isList;
    };
    TeachersComponent.prototype.blackBoard = function () {
        this.isBlackboard = !this.isBlackboard;
        this.title = "Hello World";
        this.mainService.bBonTitle(this.title);
        var obj = { bb: this.isBlackboard, room: this.roomData['room'] };
        this.mainService.Blackboard(obj);
    };
    TeachersComponent.prototype.chat = function () {
        this.isChat = !this.isChat;
        var obj = { chat: this.isChat, room: this.roomData['room'] };
        this.mainService.Chat(obj);
    };
    TeachersComponent.prototype.listPeople = function (event) {
        console.log('teacher', event);
    };
    TeachersComponent.prototype.chatEvent = function (e) {
        var _this = this;
        console.log('teacherEvent', e);
        this.Obs$ = new rxjs_1.Observable(function (observer) {
            observer.next(e);
        });
        this.ObsSubc = this.Obs$.subscribe(function (user) { return _this.chatEvents = user; });
    };
    TeachersComponent.prototype.ngOnDestroy = function () {
        this.ObsSubc.unsubscribe();
    };
    TeachersComponent = __decorate([
        core_1.Component({
            selector: "app-teachers",
            templateUrl: "./teachers.component.html",
            styleUrls: ["./teachers.component.css"]
        })
    ], TeachersComponent);
    return TeachersComponent;
}());
exports.TeachersComponent = TeachersComponent;
