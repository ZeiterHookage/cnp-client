"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.PipeImgPipe = void 0;
var core_1 = require("@angular/core");
var PipeImgPipe = /** @class */ (function () {
    function PipeImgPipe(socket) {
        this.socket = socket;
    }
    PipeImgPipe.prototype.transform = function (pipeImg, id) {
        var pipeArray = [];
        pipeArray.push(pipeImg);
        console.log("pipeArray", pipeArray);
        var _loop_1 = function (ids) {
            if (ids !== undefined) {
                return { value: "http://localhost:3000/image/peopleImage/" + ids['img'] };
            }
            else {
                //return `http://localhost:3000/image/peopleImage/pic`;
                this_1.socket.emit('idChat', ids);
                this_1.socket.on('idChat', function (id) {
                    console.log('ids', id);
                    if (ids !== undefined) {
                        return "http://localhost:3000/image/peopleImage/" + ids['img'];
                    }
                    else {
                        return "http://localhost:3000/image/peopleImage/pic";
                    }
                });
            }
        };
        var this_1 = this;
        //let ids = pipeArray.filter((idImg) => idImg === id);
        for (var _i = 0, pipeArray_1 = pipeArray; _i < pipeArray_1.length; _i++) {
            var ids = pipeArray_1[_i];
            var state_1 = _loop_1(ids);
            if (typeof state_1 === "object")
                return state_1.value;
        }
    };
    PipeImgPipe = __decorate([
        core_1.Pipe({
            name: "pipeImg"
        })
    ], PipeImgPipe);
    return PipeImgPipe;
}());
exports.PipeImgPipe = PipeImgPipe;
