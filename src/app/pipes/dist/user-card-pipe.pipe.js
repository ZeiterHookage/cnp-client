"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.UserCardPipePipe = void 0;
var rxjs_1 = require("rxjs");
var core_1 = require("@angular/core");
var UserCardPipePipe = /** @class */ (function () {
    function UserCardPipePipe() {
        this.data$ = new rxjs_1.Subject();
    }
    UserCardPipePipe.prototype.transform = function (pipeImg, id) {
        this.data$.subscribe(console.log);
        this.data$.next(pipeImg);
        if (pipeImg !== undefined) {
            return "http://localhost:3000/image/peopleImage/" + pipeImg['img'];
        }
        else {
            return "http://localhost:3000/image/peopleImage/pic";
        }
    };
    UserCardPipePipe = __decorate([
        core_1.Pipe({
            name: 'userCardPipe'
        })
    ], UserCardPipePipe);
    return UserCardPipePipe;
}());
exports.UserCardPipePipe = UserCardPipePipe;
