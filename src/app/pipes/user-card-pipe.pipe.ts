import { Subject } from 'rxjs';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userCardPipe'
})
export class UserCardPipePipe implements PipeTransform {
data$: Subject<any>= new Subject<any>();
  transform(pipeImg: string, id: string): string {
    this.data$.subscribe(console.log);
    this.data$.next(pipeImg);
      if(pipeImg !== undefined){
        return `http://localhost:3000/image/peopleImage/${pipeImg['img']}`;
      }else{
        return `http://localhost:3000/image/peopleImage/pic`;
      }

  }

}
