import { MainServicesService } from 'src/app/services/main-services.service';
import {
  Component,
  OnInit,
  Input,
  DoCheck,
  OnDestroy,
  AfterViewInit,
  Output,
  EventEmitter,
  ElementRef,
} from "@angular/core";
import { Observable, Subject, Subscription } from "rxjs";

@Component({
  selector: "app-user-card",
  templateUrl: "./user-card.component.html",
  styleUrls: ["./user-card.component.css"],
})
export class UserCardComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() dataChat: any;
  @Input() message: any;
  @Output() DOMdata: EventEmitter<HTMLElement> = new EventEmitter();
  myObservableArray: Observable<any>;
  data: Subject<any> = new Subject();
  subscription: Subscription = new Subscription();
  simpleMessageClass: any;

  mainText: any[] = [];
  mainTexting: any[] = [];
  classItems: string;
imgData:any;
  constructor(private mainService :MainServicesService) {
    this.getDataChat();
  }

  ngOnInit(): void {
    console.log(this.dataChat, this.message);
    this.imgData = this.mainService.getTaRR();
  }
  ngAfterViewInit() {
    this.simpleMessageClass = document.getElementsByTagName("p");
    this.DOMdata.emit(this.simpleMessageClass);
  }

  getDataChat() {
    if (!this.myObservableArray) {
      this.myObservableArray = this.getData();
      this.subscription = this.myObservableArray.subscribe();
    }
  }
  getData(): Subject<any> {
    const interval = setInterval(() => {
      this.mainText = this.dataChat.concat(this.message);
      this.mainText = this.mainText.sort(
        (a, b) => new Date(a.date).getTime() - new Date(b.date).getTime()
      );
      this.data.next(this.mainText);
      this.subscription = this.data.subscribe();
    },500);
    // tslint:disable-next-line:no-unused-expression
    () => clearInterval(interval);
    return this.data;
  }

  trackByFn(index: number, item: any): any {
    return index - 1;
  }
  ngOnDestroy() {
    this.mainText = [];
    this.subscription.unsubscribe();
  }
}
