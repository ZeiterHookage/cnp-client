
import { BrowserModule } from '@angular/platform-browser';
import {APP_BASE_HREF} from '@angular/common';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';

// Routes
import{APP_ROUTING} from './routes';

// ngx-socket-io

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = {url: environment.wsUrl, options: { origin: '*', transport : ['websocket'] }};
// components
import { NavbarComponent } from './components/navbar/navbar.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AdministrationComponent } from './components/administration/administration.component';
import { HumanResorcesComponent } from './components/human-resorces/human-resorces.component';
import { SupervisionComponent } from './components/supervision/supervision.component';
import { SupportComponent } from './components/support/support.component';
import { TeachersComponent } from './components/teachers/teachers.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ChatScreenComponent } from './components/chat-screen/chat-screen.component';
import { BlackboardComponent } from './components/blackboard/blackboard.component';
import { StudentComponent } from './components/student/student.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { SchoolComponent } from './components/school/school.component';
import {FormsModule} from '@angular/forms';
//sweet alert
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

//font-awesome


import { environment } from '../environments/environment';

//http module
import {HttpClientModule} from '@angular/common/http';
import { AdminGuard, HumanResorcesGuard, RegisterGuard, SchoolGuard, SettingsGuard, SupervisionGuard, TeachersGuard } from './services/Main.guard';
import { PoliticsComponent } from './components/politics/politics.component';

// virtual scrolling
import {ScrollingModule} from '@angular/cdk/scrolling';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { UserCardComponent } from './userCard/user-card/user-card.component';
import { VirtualScrollComponent } from './components/virtual-scroll/virtual-scroll.component';
import { VideosComponent } from './components/videos/videos.component';
import { SafeHtmlPipe } from './pipes/values.pipe';
import { MainServicesService } from './services/main-services.service';
import { BlackboardToolbarComponent } from './components/blackboard-toolbar/blackboard-toolbar.component';
import { SliderComponent } from './components/slider/slider.component';

// slider
import { NgxBootstrapSliderModule } from 'ngx-bootstrap-slider';
import { ImageComponent } from './components/image/image.component';
import { BbtoolbarComponent } from './components/bbtoolbar/bbtoolbar.component';
import { GradientsComponent } from './components/gradients/gradients.component';
import { PipeImgPipe } from './pipes/pipe-img.pipe';
import { UserCardPipePipe } from './pipes/user-card-pipe.pipe';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AboutComponent,
    ContactComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    SchoolComponent,
    AdministrationComponent,
    HumanResorcesComponent,
    SupervisionComponent,
    SupportComponent,
    TeachersComponent,
    SettingsComponent,
    ChatScreenComponent,
    BlackboardComponent,
    StudentComponent,
    PoliticsComponent,
    UserCardComponent,
    VirtualScrollComponent,
    VideosComponent,
    SafeHtmlPipe,
    BlackboardToolbarComponent,
    ColorPickerComponent,
    SliderComponent,
    ImageComponent,
    BbtoolbarComponent,
    GradientsComponent,
    PipeImgPipe,
    UserCardPipePipe

  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    SweetAlert2Module,
    FormsModule,
    HttpClientModule,
    ScrollingModule,
    DragDropModule,
  SocketIoModule.forRoot(config),
  NgxBootstrapSliderModule



  ],
  providers: [
    RegisterGuard,
     SchoolGuard,
      HumanResorcesGuard,
      SupervisionGuard,
      AdminGuard,
      TeachersGuard,
      SettingsGuard,
      MainServicesService,
      {provide: APP_BASE_HREF, useValue : '/' },
      { provide: Window, useValue: window }
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent  ]

})
export class AppModule { }
